''' Generates a GDVAE dataset file from a folder of mesh files '''

import os, sys, numpy as np, pickle
from tasp import FullTriangleMesh
from tasp.projects.IEVAE import DataHandling as DH
from tasp.io.utils import read_numpy_binary
import traceback, json

#--------------------------- Changeable Parameters ---------------------------#
target_folder = sys.argv[1]
lbo_size = 40
num_point_cloud_samples = 8000
outname = 'sursmpl' # 'smal' # 'smal' # 'faust'
metadata_file = '' # 'Datasets/surreal-smpl-pose_beta_data.json' 
infile_type = '.ply'
verbose = True
# Change the contents of these functions per dataset
# Should return meta-info dict per one datum
def meta_info(filename, metacong=None):
    if outname == 'faust':
        return { 'filename' : filename }
    elif outname == 'smal' or outname == 'sursmpl':
        return metacong[filename]
    else:
        return None
def is_train(filename):
    if outname == 'faust':
        return True
    elif outname == 'sursmpl':
        if filename.startswith('v'):
            return False
        return True
    elif outname == 'smal':
        number = int( filename[ filename.rfind('-') + 1 : -4 ] )
        if number >= 3000: # 3000 training examples per class
            return False
        else: # 3000 to 3199 are test examples
            return True
    else:
        print('Unrecognized option', outname)
        sys.exit(0)

##############################################################################

# Get the files in the folder
files = [f for f in os.listdir(target_folder) if f.endswith(infile_type)]
print('Found %d files\n' % len(files), 'E.g.,', files[0:5])

# Read in the meta-info conglomerate
if metadata_file.endswith('.npy'):
    with open(metadata_file, 'rb') as mdfh:
        # metac = pickle.load(mdfh)
        metac = read_numpy_binary(mdfh)
elif metadata_file.endswith('.json'):
    with open(metadata_file, 'r') as mdfh:
        metac = json.load(mdfh)  #read_numpy_binary(mdfh)

# For each mesh, read it in, compute the spectrum, and sample a point cloud
collection_train = []
collection_test = []
fail_count = 0
for i, target in enumerate(files):
    fname = os.path.join(target_folder, target)
    cmesh = FullTriangleMesh.readFromFile( fname, print_info=False, verbose=False )
    cmesh.name = target #'dyna-index-' + str(index)
    print('\n',i, cmesh.name)
    meta_info_curr = meta_info(target, metac)
    #print(meta_info_curr)
    damp_eigensys = (outname == 'faust')
    try:
        # Compute spectral data and sample point cloud
        datum = DH.IEVAE_Datum(cmesh,
                               num_sampled_points      = num_point_cloud_samples,
                               lbo_spectrum_size       = lbo_size,
                               rel_dirac_spectrum_size = None, # dirac_s,
                               precomputed_relDirac    = None,
                               extra_data              = meta_info_curr,
                               #marq_lev_coef           = 5e-6,
                               node_areas_pert         = 1e-7,
                               #lbo_spec_tol            = 1e-6,
                               #lbo_spec_max_iters      = 20000,
                               allow_mat_perturbations = damp_eigensys,
                               verbose                 = verbose)
        if is_train(target):
            print('\tAdding to training set')
            collection_train.append( datum )
        else:
            print('\tAdding to testing set')
            collection_test.append( datum )
    except Exception as err:
        print('\n' + '='*20 + '\n', 'Failure detected!', '\n' + '='*20 + '\n')
        print(err)
        traceback.print_tb(err.__traceback__)
        fail_count += 1

# Create dataset
curr_name_train = outname + '-train' + '-GDVAE_dataset.pickle'
curr_name_test = outname + '-test' + '-GDVAE_dataset.pickle'
print('Generating complete datasets')
dataset_train = DH.IEVAE_Dataset(collection_train)
if len(collection_test) >= 1:
    dataset_test = DH.IEVAE_Dataset(collection_test)
print('\tSaving dataset file')
dataset_train.save_to_file(curr_name_train)
print('\tSaved to', curr_name_train)
if len(collection_test) >= 1:
    dataset_test.save_to_file(curr_name_test)
    print('\tSaved to', curr_name_test)



#
