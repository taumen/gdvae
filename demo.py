import os, sys, argparse, torch, numpy as np
from tasp.projects.IEVAE.GDVAE import GDVAE
from tasp.projects.IEVAE import DataHandling as DH
from tasp.projects.IEVAE.gdvae_autoencoder import Autoencoder_IEVAE_fixedDec, run_training
from tasp.structures.PointCloud import PointCloud

def main():
    # Parser
    parser = argparse.ArgumentParser(description='GDVAE Entry Point')
    parser.add_argument('options_choice', type=str, 
                        choices=[ 'vis_smal',       'vis_smpl',   'vis', 
                                  'print_smpl',     'print_smal', 
                                  'train_mnist_ae', 'train_mnist_vae',
                                  'vis_mnist_vae' ],
                        help='Name of options set')
    parser.add_argument('--ae_model',  type=str, help='Path to AE model')
    parser.add_argument('--vae_model', type=str, help='Path to VAE model')
    parser.add_argument('--key',       type=str, help='Key name for settings')
    parser.add_argument('--tag',       type=str, help='Tag used for saving VAE model')
    parser.add_argument('--gpu_id',    type=int, help='Device ID', default=0)
    args = parser.parse_args()

    # Defaults 
    mdls_dir = 'models'
    data_dir = 'data'
    join     = lambda s: os.path.join(mdls_dir, s)
    smal_ae  = 'smal2-031919-AE.pt'
    smal_vae = 'smal-GDVAE-032019-T4.pt'
    smpl_ae  = 'sursmpl2-031919-AE.pt'
    smpl_vae = 'sursmpl-GDVAE-032019-T8.pt'

    # Printing model params
    from test_AE import main_print as ae_print
    if args.options_choice == 'print_smpl':
        ae_print(  join(smpl_ae) )
        vae_print( join(smpl_vae) )
    if args.options_choice == 'print_smal':
        ae_print(  join(smal_ae) )
        vae_print( join(smal_vae) )

    # Visualization
    if args.options_choice == 'vis_smal':
        main_vis( join(smal_ae), join(smal_vae), 'smal' )
    if args.options_choice == 'vis_smpl':
        main_vis( join(smpl_ae), join(smpl_vae), 'smpl' )
    if args.options_choice == 'vis':
        assert ( (not args.vae_model is None) and 
                 (not args.ae_model  is None) and
                 (not args.key       is None)
               ), "python demo.py vis --ae_model [ae_model] --vae_model [vae_model] --key [key]"
        main_vis( args.ae_model, args.vae_model, args.key )
    if args.options_choice == 'vis_mnist_vae':
        assert ( (not args.vae_model is None) and 
                 (not args.ae_model  is None) 
               ), "python demo.py vis_mnist_vae --ae_model [ae_model] --vae_model [vae_model]"
        from gdvae_test import DATASETS, DATASETS_LOCATION
        main_vis( args.ae_model, args.vae_model, 'mnist', 
                  os.path.join(DATASETS_LOCATION, DATASETS['mnist']['test']) )

    # Training
    if args.options_choice == 'train_mnist_ae':
        train_ae('mnist', args.gpu_id)
    if args.options_choice == 'train_mnist_vae':
        assert not args.ae_model is None, "You must specify the AE model path"
        train_vae(args.ae_model, 'mnist', args.gpu_id, tag=args.tag)

#----------------------------------------------------------------------------#

def train_ae(key, gpu_id):
   from test_AE import main_train
   main_train(key, gpu_id)

def train_vae(AE_model_path, key, gpu_id, tag):
    from gdvae_test import main_train
    main_train(AE_model_path, key, gpu_id, tag=tag)

VIS_SETTINGS = {
        'smal' : {
            'R' : np.array([[1.0,           0.0,              0.0],
                            [0, np.cos(np.pi/2), -np.sin(np.pi/2)],
                            [0, np.sin(np.pi/2),  np.cos(np.pi/2)]]),
            'sampling_hpad' : 0.25
        },
        'mnist' : {
            'R' : np.array([[1.0, 0,   0],
                            [0,   1.0, 0],
                            [0,   0,   1.0]]),
            'sampling_hpad' : 0.1
        },
        'smpl' : {
            'R' : np.array([[1.0, 0,   0],
                            [0,   1.0, 0],
                            [0,   0,   1.0]]),
            'sampling_hpad' : 0.1
        }
}

def vae_print(VAE_model):
    print('Loading VAE model', VAE_model)
    VAE_model = GDVAE.load(VAE_model, mode='eval')
    print('Training Dict')
    print(VAE_model.training_params)
    print('Architecture')
    print(VAE_model)
    print('Dimensionalities')
    VAE_model.print_dims()
    if hasattr(VAE_model, 'OPTS'):
        print('Options')
        print(VAE_model.OPTS)
    if hasattr(VAE_model, 'EPOCHAL_LOSS_VALUES'):
        print('Recorded loss values present')
        names = VAE_model.EPOCHAL_LOSS_NAMES
        vals = VAE_model.EPOCHAL_LOSS_VALUES
        print('\tLength', len(vals))
        print('\tLength per entry', len(vals[0]))
        print('\tNames', names)

def load_models(AE_model, VAE_model):
    print('Loading AE model', AE_model)
    AE_model = Autoencoder_IEVAE_fixedDec.load(AE_model, mode='eval')
    print('Loading VAE model', VAE_model)
    VAE_model = GDVAE.load(VAE_model, mode='eval')
    print('Converting models to cpu')
    AE_model = AE_model.cpu()
    VAE_model = VAE_model.cpu()
    return AE_model, VAE_model

def main_vis(AE_model, VAE_model, key, dfile=None):
    AE_model, VAE_model = load_models(AE_model, VAE_model)
    denovo_main_vis(AE_model, VAE_model, key)
    interp_vis(AE_model, VAE_model, key)
    if not dfile is None:
        interp_vis(AE_model, VAE_model, key, datafile=dfile)

def denovo_main_vis(AE_model, VAE_model, key):
    vsettings = VIS_SETTINGS[key]
    rot_dim = VAE_model.rotation_dim
    ext_dim = VAE_model.extrinsic_dim
    int_dim = VAE_model.intrinsic_dim
    RE_dim = rot_dim + ext_dim
    fixed_R = vsettings['R']

    SHOW_DE_NOVO_SAMPLINGS = True
    if SHOW_DE_NOVO_SAMPLINGS:
        NROWS, NCOLS, use_zero_for_zR = 1, 3, True
        zero_Q = False
        print('\n--Visualizing de novo samplings--')
        n_samples = NROWS * NCOLS
        latent_prior_vectors = torch.randn(n_samples, VAE_model.LD)
        if use_zero_for_zR: latent_prior_vectors[:, 0 : rot_dim ] = 0.0
        latent_PCs = VAE_model.decode(z=latent_prior_vectors)
        if zero_Q: latent_PCs[:, 0:4] = 0.0
        rett = lambda a: a.detach().cpu().numpy()
        PCs = rett( AE_model.decode(latent_PCs) )
        pc_ind, rows = 0, []
        for ii in range(NROWS):
            row = []
            for jj in range(NCOLS):
                curr_pc = PointCloud( PCs[pc_ind,:,:] ).rotate(fixed_R)
                curr_pc.simple_color_by_coords(coord_index=2,
                            color_index=0, oth_value=(0.4,0.5))
                pc_ind += 1
                row.append( curr_pc )
            rows.append(row)
        finalee = PointCloud.grid_combine( rows, 
                        h_padding = vsettings['sampling_hpad'], 
                        align_y_mins = True )
        finalee.display(verbose=False)
        save_mitsuba_dn = False
        if save_mitsuba_dn:
            finalee.writeToMitsubaFile(
                    'denovo-samples-' + str(KEY) + '.xml',
                    overwrite_output=True)

def interp_vis(AE_model, VAE_model, key, datafile=None):
    ud = False
    if not datafile is None:
        print('Loading', datafile)
        dataset_object = DH.IEVAE_Dataset.read_from_file(datafile, file_filter=None)
        ud = True
    NUM_INTERPS_TO_VIS = 3
    NR, NC = 4, 5
    use_zero_for_zr_in_interps = True
    vsettings = VIS_SETTINGS[key]
    rot_dim = VAE_model.rotation_dim
    ext_dim = VAE_model.extrinsic_dim
    int_dim = VAE_model.intrinsic_dim
    RE_dim = rot_dim + ext_dim
    if ud: # Utilize encoded data point (i.e., mu(X))
        from gdvae_test import SETTINGS as S
        n_sample_points_per_shape = S[key]['N_AE_samps']
        only_rotate_z_axis = (S[key]['rot_axis'] == 'z')
        point_clouds, dataset_indices, R = dataset_object.sample_batch_unifrand(
                                            NUM_INTERPS_TO_VIS * 2,
                                            n_sample_points_per_shape,
                                            rotate = True, #rotate_training_data,
                                            max_rotation_angle = 0.0, #max_rotation_angle,
                                            z_axis_only = only_rotate_z_axis,
                                            y_axis_only = not only_rotate_z_axis)
    else: # Utilize random latent sample 
        latent_prior_vectors = torch.randn(NUM_INTERPS_TO_VIS*2, VAE_model.LD)
    fixed_R = vsettings['R']
    subsample_pre_display = False # whether to subsample
    subsample_pre_disp_n  = 1100 # Number of points per model to subsample to
    interp_h_padding = 0.05 
    interp_v_padding = 0.05
    def to_pc(p, color=None):
        p = PointCloud( p.squeeze(0).detach().cpu().numpy() ).rotate(fixed_R)
        if color is None: # Regular
            p.simple_color_by_coords(2, 0, oth_value=(0.4,0.5)) # Used in paper/poster
        else: # Darker/corner
            p.simple_color_by_coords(2, 2, oth_value=(0.1,0.1)) # Used in paper/poster
        return p
    # Draw two random shapes, and show intrinsic/extrinsic interpolations
    print('\n--Visualizing Interpolations from %s--' % ('posterior' if ud else 'prior'))
    print('Note: vertical axis is extrinsic; horizontal is intrinsic')
    for i in range(NUM_INTERPS_TO_VIS):
        print('On %s sample %d' % ( ('data' if ud else 'random'), i ))
        ind1, ind2 = 2*i, 2*i + 1
        if ud:
            from torch.autograd import Variable
            L1 = VAE_model.encode( AE_model.encode( Variable(
                        torch.FloatTensor(point_clouds[ind1,:,:] ).unsqueeze(0)
                    ) ), return_mu_only = True )
            L2 = VAE_model.encode( AE_model.encode( Variable(
                        torch.FloatTensor(point_clouds[ind2,:,:] ).unsqueeze(0)
                    ) ), return_mu_only = True )
        else:
            L1 = latent_prior_vectors[ind1,:].unsqueeze(0)
            L2 = latent_prior_vectors[ind2,:].unsqueeze(0)
        if use_zero_for_zr_in_interps:
                L1[ 0, 0 : rot_dim ] = 0.0
                L2[ 0, 0 : rot_dim ] = 0.0
        # Interpolate zEs and zIs
        zE1, zE2 = L1[ 0, 0 : RE_dim ], L2[ 0, 0 : RE_dim ]
        zI1, zI2 = L1[ 0, RE_dim : ],   L2[ 0, RE_dim : ]
        t_vals_E = torch.linspace(start=0, end=1, steps=NR)
        t_vals_I = torch.linspace(start=0, end=1, steps=NC)
        zE_vals = [ torch.lerp(zE1, zE2, t) for t in t_vals_E ]
        zI_vals = [ torch.lerp(zI1, zI2, t) for t in t_vals_I ]
        # Decode in a grid
        decoded_pc_rows = [
            [   to_pc(
                    AE_model.decode(
                        VAE_model.decode(
                            z = torch.cat( (zE_vals[R].unsqueeze(0),
                                            zI_vals[C].unsqueeze(0)
                                         ), dim=1)
                        )
                    ),
                    color = 'darker' if (
                            (C == 0 and R == 0) or (C == NC-1 and R == NR-1)
                        ) else None
                )
                for C in range(NC)
            ]
            for R in range(NR)
        ]
        if subsample_pre_display:
            print('\tDownsampling from %d to %d' %
                  (decoded_pc_rows[0][0].num_points(), subsample_pre_disp_n))
            for ii in range( len(decoded_pc_rows) ):
                sublist = decoded_pc_rows[ii]
                for jj in range( len(sublist) ):
                    curr = sublist[jj]
                    decoded_pc_rows[ii][jj] = curr.random_subsample_obj(
                                                    subsample_pre_disp_n)
        # Display the PCs
        combined_vis_grid = PointCloud.grid_combine(
                                decoded_pc_rows,
                                h_padding=interp_h_padding,
                                v_padding=interp_v_padding,
                                align_y_mins=True)
        combined_vis_grid.display(verbose=False)
        save_mitsuba_dn = False
        if save_mitsuba_dn:
            #combined_vis_grid.set_radius(m_factor=MF)
            combined_vis_grid.writeToMitsubaFile(
                    str(interp_tag) + '-interp-' + str(KEY) + '-'
                        + str(i) + '.xml',
                    camera_lift=0.5,
                    lookAt_lowering=0.0,
                    img_size=1000,
                    #subsample_points=750,
                    overwrite_output=True)

#------------------------#
if __name__ == '__main__':
    main()
#------------------------#


#
