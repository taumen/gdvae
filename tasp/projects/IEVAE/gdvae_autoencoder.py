# from __future__ import print_function
import torch, time, os, sys, numpy as np
from torch import nn, optim
from torch.nn import functional as F
from torch.autograd import Variable
from .DataHandling import IEVAE_Dataset
from ...learning.Utils import partial_chamfer_and_Hausdorff_over_sets, load_mnist, count_parameters as n_parms, full_chamfer_over_sets, full_chamfer_and_Hausdorff_over_sets
#from ...learning.PointNet import PointNetBase
from ...learning.SimplePointNet import SimplePointNet

#from .ievae_encoder import IEVAE_Encoder_PointNet_wganlp, IEVAE_Encoder_wganlp_old
#from .ievae_decoder import IEVAE_Decoder_fixed_mapper, IEVAE_Decoder_manifold_sampler

class Autoencoder_IEVAE_fixedDec(nn.Module):
    '''
    Autoencoder for the Intrinsic-extrinsic VAE.
    '''

    def __init__(self,
        num_input_points  : int,
        num_output_points : int,
        latent_dim        : int,  # = 250,
        conv_dim          : int,  # = 400,
        conv_layer_sizes  : list, # = None,
        fc_layer_sizes    : list,
        transformer_positions : list,
        DLS               : list, # = None
        decoder_normalization
        ):

        super(Autoencoder_IEVAE_fixedDec, self).__init__()

        ### Data dimensionalities and Settings ###
        self.latent_dimensionality = latent_dim
        self.conv_dim = conv_dim
        self.n_out = num_output_points
        assert decoder_normalization in [None, 'none', 'batch', 'layer']
        self.dec_norm = ( None if decoder_normalization in [None, 'none']      else
                          ( nn.BatchNorm1d if decoder_normalization == 'batch' else
                            nn.LayerNorm)  )
        ### Encoder ###
        self.encoder = SimplePointNet(
                            latent_dimensionality = latent_dim,
                            convolutional_output_dim = conv_dim,
                            conv_layer_sizes = conv_layer_sizes,
                            fc_layer_sizes = fc_layer_sizes,
                            transformer_positions = transformer_positions,
                            end_in_batchnorm = False
        ) # Does it make sense to BN the quat?! It has an extra DOF so should be ok.

        ### Decoder ###
        self.dec_sizes = [self.latent_dimensionality - 4] + [a for a in DLS]
        self.decoder_layers = nn.ModuleList([
            (  nn.Sequential(
                  nn.Linear(self.dec_sizes[i], self.dec_sizes[i+1]),
                  # no norm in Achlioptas et al
                  nn.ReLU()
               ) if self.dec_norm is None else (
               nn.Sequential(
                  nn.Linear(self.dec_sizes[i], self.dec_sizes[i+1]),
                  self.dec_norm( self.dec_sizes[i+1] ),
                  nn.ReLU()
               ) )
            )
            for i in range(len(self.dec_sizes)-1)
            ] + [ nn.Linear(self.dec_sizes[-1], self.n_out * 3) ]
        )

    ### Autoencoding methods ###

    def encode(self, x): #input_point_clouds):
        # Input: B x N x 3
        # If permuting the dims is needed, this must be done by the encoder.
        return self.encoder(x)

    def decode(self, z, n=None, unrotated_too=False):
        B = z.shape[0]
        R = quat2mat(z[:, 0:4]) # Latent quaternion -> rotation matrix
        P = z[:, 4:] # Remaining latent representation
        # Run through the fully connected decoder
        for layer in self.decoder_layers: P = layer(P)
        # Reshape into a point cloud
        P = P.view(B, self.n_out, 3)
        if unrotated_too: return torch.bmm(P, R), P
        return torch.bmm(P, R)

    def forward(self, target, n=None, unrotated_too=False, return_enc_too=False,
                use_data_parallel=False, data_inds=None, use_rc=True):
        if use_data_parallel:
            device = target.device
            enc = self.encode(target)
            P_hat = self.decode(enc, unrotated_too=False)
            LD, s = self.loss_function_direct(P_hat, target)
            if use_rc:
                Lrc = self.compute_rc_loss(data_inds, enc, device, self)
                L = LD + Lrc
                COM = torch.stack( [L] + [ell for ell in s] + [Lrc] ).unsqueeze(0)
            else:
                COM = torch.stack( [LD] + [ell for ell in s] ).unsqueeze(0)
            return COM 
        if return_enc_too:
            enc = self.encode(target)
            return self.decode(enc, n=n, unrotated_too=unrotated_too), enc 
        return self.decode(self.encode(target), n=n, unrotated_too=unrotated_too)

    ### Mode switching ###

    def to_eval_mode(self):
        self.eval()
        return self

    def to_train_mode(self):
        self.train()
        return self

    ### Saving and Loading Methods ###

    def save(self, path_to_save):
        torch.save(self, path_to_save)

    def load(path_to_load, gpu_to_cpu=True, mode='eval'): # Static
        assert mode in ['eval', 'train'], 'Mode must be eval or train'
        mc = lambda m: m.to_eval_mode() if mode == 'eval' else m.to_train_mode()
        if gpu_to_cpu:
            return mc(torch.load(path_to_load,
                      map_location=lambda storage,
                      location: storage))
        return mc(torch.load( path_to_load ))

    def save_state(self, path_to_save):
        torch.save(self.state_dict(), path_to_save)

    def load_state(self, path_to_load, mode='eval', gpu_to_cpu=True): # Non-static
        assert mode in ['eval', 'train'], 'Mode must be eval or train'
        if not gpu_to_cpu:
            self.load_state_dict(torch.load(path_to_load))
        else:
            self.load_state_dict(torch.load(path_to_load,
                map_location=lambda storage, location: storage))
        if mode == 'eval': self.eval()
        else:              self.train()

### Training Loop ###

def run_training(AE_model,
                 dataset_files,              # List of files holding the data
                 path_to_save_model,         # Path to file in which to save the model
                 use_data_parallel,
                 ### Training parameters ###
                 num_epochs,                 # Number of epochs (passes over all data files)
                 batch_size,                 # The size of individual batches (number of shapes)
                 learning_rate,              #
                 dataset_coverings,          # Num times a dataset file is covered before loading the next one
                 n_loss_samples,             # NOT BEING USED IN THIS VERSION
                 l2_reg_weight_decay,        # Weight decay (L2 reg) penalty strength
                 ### Meta-parameters ###
                 print_every,                #
                 save_every,                 #
                 allow_initial_overwrites,   # Whether to ignore if the save_path is already taken
                 exit_early_after_n_batches, #
                 device,                     # GPU object
                 num_sub_batches,            # Number of Sub-batches to split each batch into
                 ### Weight constants ###
                 alpha_1,                    # Chamfer max-avg weighting
                 alpha_2,                    # Approximate Hausdorff max-avg weighting
                 chamfer_weight,
                 hausdorff_weight,
                 ### Rotational Consistency  ###
                 use_rc,
                 rc_coef,
                 rc_sample_size,
                 ### Training data params ###
                 file_filter,
                 preload_files,
                 rotate_training_data,
                 only_rotate_z_axis,
                 only_rotate_y_axis,
                 max_rotation_angle
                ):
    '''
    Given a list files, need to load them one at a time. Each epoch loops over all the files.
    Each file is processed by sampling batches until we expect to have covered the file dataset_coverings times.

    Saves the model to path_to_save_model every save_every iterations.

    Can get a validation set by always setting aside the first n_val shapes of each file. (?)

    dataset_files: list of data files
    '''

    ### HACK ###
    # Specify data parallel devices ids with e.g. --devices=2,3
    args = sys.argv
    sdevices = None
    sig = '--devices'
    for a in args:
        if a.startswith(sig):
            p = a.split('=')[1].split(',')
            sdevices = [ int(qqq) for qqq in p ]
            print('USING MANUALLY SPECIFIED DEVICE IDS:', sdevices)
    if not sdevices is None:
        device = sdevices[0]
        print('\tManually changing device to be GPU ID', device)
    ### End Hack ###

    AE_model.training_params = locals()

    # +++++++++++++++++++++++++++++++++++++++ Pre computations/settings  +++++++++++++++++++++++++++++++++++++++ #
    # Put model in training mode
    AE_model.train()
    # Move to given device
    AE_model.to(device)
    AE_model.encoder.move_eye(device)
    # Converter to pytorch
    #as_pt_var = lambda s: Variable( torch.FloatTensor(s), volatile=True ) # Careful using if gradient needed!
    #as_pt_var = lambda s: torch.as_tensor(s)
    # Whether to print each time we change files
    print_file_change = True
    # Helper for printing
    as_str = lambda s: '%.3f' % ( s if type(s) is float
                        else float( s.detach().cpu().numpy() ) )
    # Optimizer
    optimizer = optim.Adam(AE_model.parameters(), lr=learning_rate,
                           weight_decay=l2_reg_weight_decay)
    # Number of sub-batches
    n_sub_batches = num_sub_batches
    # Prevent overwriting
    if not allow_initial_overwrites:
        msg = 'File ' + path_to_save_model + ' exists.'
        assert not os.path.isfile(path_to_save_model), msg
    # Numerical stability epsilon
    eps = 0.000001
    one_plus_eps = 1.0 + eps

    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++#

    ##### Pre-Training Info #####
    print('\n' + ('-' * 75) + '\nIEVAE Training Info\n' + ('-' * 75))
    print('NumEpochs:', num_epochs)
    print('Max-Weighted Chamfer coefficient:', alpha_1)
    # print('Chamfer weight: %f, Hausdorff weight: %f' % (chamfer_weight, hausdorff_weight))
    print('Print_every/save_every: %d/%d' % (print_every, save_every))
    print('Num expected dataset coverings:', dataset_coverings)
    print('Learning rate', learning_rate)
    print('Batch Size', batch_size)
    print('Saving to', path_to_save_model)
    print('Num Sampled Points:\n\tPoints from input =', AE_model.n_out)
#    print('\tNumber of reference samples to compute loss =', n_loss_samples)
    print('\tNumber of points for decoder to sample =', AE_model.n_out)
    print('Expanded tensor size in decoder:', batch_size*AE_model.n_out*3)
    print('Device in use', device)
    print('Latent dimensionality', AE_model.latent_dimensionality)
    if rotate_training_data: print('Data rotation: only z? ' + str(only_rotate_z_axis) +
                                    ", only y? " + str(only_rotate_y_axis) +
                                   '. max_angle: %.2f' % max_rotation_angle)
    print('-'*75)

    N_OUT = AE_model.n_out
    ##### AE Loss Functions #####
    # R in B x 3 x 3 here is the tensor of true rotation matrices of the data
    # over the batch.
    def loss_function_direct(points_hat, points_orig):
        ### Coverage of the reference & closeness of each gen to the ref ###
        T_r, T_g, H_r, H_g = full_chamfer_and_Hausdorff_over_sets(points_hat, points_orig.detach())
        cham_loss = alpha_1 * torch.max(T_r, T_g) + (1 - alpha_1) * (T_r + T_g) / 2.0
        hausdorff_loss = alpha_2 * torch.max(H_g, H_r) + (1 - alpha_2) * (H_r + H_g) / 2.0
        ### Combined loss ###
        combined_loss = (chamfer_weight * cham_loss +
                         hausdorff_weight * hausdorff_loss)
        return (combined_loss, (cham_loss, hausdorff_loss, T_r, T_g, H_r, H_g))
    AE_model.loss_function_direct = loss_function_direct
    # ---- End AE loss function ---- #
            
    # Define how to compute the rc loss
    def compute_rc_loss(dinds, enc, device, AEM):
        rc_inds = dinds[0 : rc_sample_size]
        X_1 = enc[0 : rc_sample_size, 4 : ] # non-quat X portion
        rc_inds = rc_inds.cpu().numpy()
        raw_alt_rot, _, R = dataset_object.sample_batch(
                                        rc_sample_size, N_OUT,
                                        rotate=rotate_training_data,
                                        max_rotation_angle=max_rotation_angle,
                                        z_axis_only=only_rotate_z_axis,
                                        y_axis_only=only_rotate_y_axis,
                                        prechosen_indices=rc_inds)
        X_2 = AEM.encode( torch.from_numpy( raw_alt_rot.astype(np.float32, copy=False) 
                                           ).to(device).detach() )[:, 4 : ]
        loss_rc = rc_coef * ( (X_1 - X_2)**2 ).mean()
        return loss_rc
    AE_model.compute_rc_loss = compute_rc_loss

    #
    if use_data_parallel and torch.cuda.device_count() > 1:
        print("Have access to", torch.cuda.device_count(), "GPUs")
        if not sdevices is None: # if specified devices are present
            print('Using', len(sdevices), 'of them')
            AE_model = nn.DataParallel(AE_model, device_ids=sdevices)
        else:
            AE_model = nn.DataParallel(AE_model)
        AE_model.to(device)
    
    #
    if preload_files:
        print('Preloading files')
        preloaded_files = {
            f_name : IEVAE_Dataset.read_from_file(f_name, file_filter=file_filter)
            for f_name in dataset_files
        }
        print('\tCompleted')

    ##### Training loop ######
    total_batch_count, total_start_time = 0, time.time()
    rc_template = "LC = %.4f, LH = %.4f, T_r = %.4f, T_g = %.4f, H_r = %.4f, H_g = %.4f, RC = %.4f"
    non_rc_template = "LC = %.4f, LH = %.4f, T_r = %.4f, T_g = %.4f, H_r = %.4f, H_g = %.4f"
    template_to_use = rc_template if use_rc else non_rc_template 
    for i in range(num_epochs):
        epoch_start_time = time.time()
        print('-' * 30, '\nEpoch', i, '\n' + '-' * 30)
        epochal_losses = []

        # Loop over the training set files
        for fi, file in enumerate(dataset_files):

            # Load the current data file
            if print_file_change: print('Loading file %d:' % fi, file)
            if preload_files: dataset_object = preloaded_files[file]
            else:             dataset_object = IEVAE_Dataset.read_from_file(file,
                                                                file_filter=file_filter)

            # Compute number of batches to run (for this file)
            n_batches = dataset_coverings * dataset_object.num_samples_to_cover_set(batch_size)
            if print_file_change: print('\tObtained', dataset_object.num_shapes, 'samples. Using NB =', n_batches)
            list_of_times = []

            # Loop over batches
            for j in range(n_batches):
                # Start timer
                batch_processing_start_time = time.time()
                # Count tracks the number of batches
                total_batch_count += 1
                # Loss holder
                loss = 0
                for ell in range(n_sub_batches):
                    # Procure a batch of data
                    #  o point_clouds = subsample of the random shapes in batch_size x shape_sample_size x 3
                    #  o dataset_indices = indices of shapes in the dataset
                    #  o the rotations used on this batch of data
                    point_clouds, dataset_indices, R = dataset_object.sample_batch(
                                            batch_size, N_OUT,
                                            rotate=rotate_training_data,
                                            max_rotation_angle=max_rotation_angle,
                                            z_axis_only=only_rotate_z_axis,
                                            y_axis_only=only_rotate_y_axis)
                    # Convert to pytorch
                    P = torch.from_numpy( point_clouds.astype(np.float32, copy=False) ).to(device).detach()
                    # Run the model on the batch
                    if use_data_parallel:
                        dataset_indices = torch.LongTensor(dataset_indices).to(device)
                        COM = AE_model.forward(P, n=N_OUT, use_data_parallel=True, use_rc=use_rc,
                                                       data_inds=dataset_indices)
                        combined_loss = COM.mean(dim=0)
                        loss_curr = combined_loss[0]
                        loss_subparts  = combined_loss[1:].cpu().detach().numpy().tolist()
                        loss = loss + loss_curr
                    else:
                        p_hat, enc = AE_model.forward(P, n=N_OUT, return_enc_too=True, use_rc=use_rc)
                        loss_curr, loss_subparts = loss_function_direct(p_hat, P)
                        loss = loss + loss_curr
                        # Enforce rotational consistency
                        if use_rc:
                            loss_rc = compute_rc_loss(dataset_indices, enc, device, AE_model)
                            loss = loss + loss_rc 

                #print('lc',loss_curr)
                #print(float(loss_curr.cpu().detach().numpy()))
                #print('ls',
                epochal_losses.append( [float(loss_curr.cpu().detach().numpy()), *loss_subparts] )
                #print('EPLS', epLs)
                #sys.exit(0)

                #-----------------------------#

                # Classic gradient descent steps
                optimizer.zero_grad() # Clear out old optimizer gradient (includes the whole network)
                loss.backward()       # Backprop gradient values
                optimizer.step()      # Apply parameter changes

                # Track processing time
                list_of_times.append( time.time() - batch_processing_start_time )
                # Display training progress
                if total_batch_count % print_every == 0:
                    tab = ' ' * 3
                    print("C%d.E%d.B%d.F%d. Train loss: %f." % (total_batch_count, i, j, fi, loss),
                          "(%.2fs/b)" % np.mean(list_of_times))
                    if use_data_parallel and use_rc:
                        print(tab, rc_template % tuple(loss_subparts) )
                    elif use_rc:
                        loss_subparts = (*loss_subparts, loss_rc)
                        print(tab, rc_template % loss_subparts)
                    else:
                        print(tab, non_rc_template % tuple(loss_subparts) )
                    list_of_times = [] # clear batches so far
                
                # Save the model, if desired
                if total_batch_count % save_every == 0:
                    print("Saving model to", path_to_save_model)
                    if use_data_parallel:
                        AE_model.module.loss_function_direct = None 
                        AE_model.module.loss_function_cham_only = None
                        AE_model.module.compute_rc_loss = None
                        ##
                        AE_model.module.save(path_to_save_model)
                        ##
                        AE_model.module.loss_function_direct = loss_function_direct 
                        #AE_model.module.loss_function_cham_only = loss_function_cham_only
                        AE_model.module.compute_rc_loss = compute_rc_loss
                    else:
                        AE_model.loss_function_direct = None 
                        AE_model.loss_function_cham_only = None
                        AE_model.compute_rc_loss = None
                        AE_model.save(path_to_save_model)
                        AE_model.loss_function_direct = loss_function_direct 
                        #AE_model.loss_function_cham_only = loss_function_cham_only
                        AE_model.compute_rc_loss = compute_rc_loss

                # Exit early if desired
                if not exit_early_after_n_batches is None:
                    if total_batch_count >= exit_early_after_n_batches: return

        # Print epoch timing stats
        ectime, tctime = time.time() - epoch_start_time, time.time() - total_start_time
        print('Epoch %d finished in %.3fs. (Total time passed = %.3fs)' % (i, ectime, tctime) )
        epLs = torch.FloatTensor(epochal_losses).mean(dim=0).cpu().detach().numpy().tolist()
        print('AVGs:', ('L = %.4f, ' + template_to_use) % tuple(epLs))
        

def quat2mat(quat):
    """
    Convert quaternion coefficients to rotation matrix.

    Args:
        quat: quaternion as a 4-tuple -- size = [B, 4]
    Returns:
        Rotation matrix corresponding to the quaternion -- size = [B, 3, 3]

    From:
       https://github.com/ClementPinard/SfmLearner-Pytorch
    Specifically:
       https://github.com/ClementPinard/SfmLearner-Pytorch/blob/master/inverse_warp.py
    MIT Licensed
    Minor modifications by TTAA
    """
    # norm_quat = torch.cat([quat[:,:1].detach()*0 + 1, quat], dim=1)
    # norm_quat = norm_quat/norm_quat.norm(p=2, dim=1, keepdim=True)
    norm_quat = F.normalize(quat, p=2, dim=1)
    w, x, y, z = norm_quat[:,0], norm_quat[:,1], norm_quat[:,2], norm_quat[:,3]
    B = quat.size(0)
    w2, x2, y2, z2 = w.pow(2), x.pow(2), y.pow(2), z.pow(2)
    wx, wy, wz = w*x, w*y, w*z
    xy, xz, yz = x*y, x*z, y*z
    rotMat = torch.stack([w2 + x2 - y2 - z2, 2*xy - 2*wz, 2*wy + 2*xz,
                          2*wz + 2*xy, w2 - x2 + y2 - z2, 2*yz - 2*wx,
                          2*xz - 2*wy, 2*wx + 2*yz, w2 - x2 - y2 + z2],
                    dim=1).view(B, 3, 3)
    return rotMat

######################################################################################################################

def main(): pass

###########################
if __name__ == '__main__':
    main()
###########################
