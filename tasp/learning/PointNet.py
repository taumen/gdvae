####################################################################################
# Implementation of PointNet in PyTorch
# Based on code from: meder411
# Accessible at: https://github.com/meder411/PointNet-PyTorch
#
# Modified by TTAA
####################################################################################

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.autograd as grad
from .PointNetTransformer import Transformer

##-----------------------------------------------------------------------------
# Class for PointNetBase. Subclasses PyTorch's own "nn" module
#
# Computes the local embeddings and global features for an input set of points
##
class PointNetBase(nn.Module):

    def __init__(self,
                 num_points,
                 global_feature_size=1024,
                 K=3,
                 device=None,
                 mlp1_sizes=(64,64),
                 mlp2_sizes=(64,128)
        ):

        # Call the super constructor
        super(PointNetBase, self).__init__()
        self.global_feature_size = global_feature_size
        self.K = 3
        self.T2_size = mlp1_sizes[1]
        self.T1_reshape = self.K * self.K
        self.T2_reshape = self.T2_size * self.T2_size

        # Input transformer for K-dimensional input
        # K should be 3 for XYZ coordinates, but can be larger if normals,
        # colors, etc are included
        self.input_transformer = Transformer(num_points,
                                             K,
                                             device=device)

        # Embedding transformer is always going to be 64 dimensional
        self.embedding_transformer = Transformer(num_points,
                                                 mlp1_sizes[1],
                                                 device=device)

        # Multilayer perceptrons with shared weights are implemented as
        # convolutions. This is because we are mapping from K inputs to 64
        # outputs, so we can just consider each of the 64 K-dim filters as
        # describing the weight matrix for each point dimension (X,Y,Z,...) to
        # each index of the 64 dimension embeddings
        self.mlp1 = nn.Sequential(
            nn.Conv1d(K, mlp1_sizes[0], 1),
            nn.BatchNorm1d(mlp1_sizes[0]),
            nn.ReLU(),
            nn.Conv1d(mlp1_sizes[0], mlp1_sizes[1], 1),
            nn.BatchNorm1d(mlp1_sizes[1]),
            nn.ReLU())

        self.mlp2 = nn.Sequential(
            nn.Conv1d(mlp1_sizes[1], mlp2_sizes[0], 1),
            nn.BatchNorm1d(mlp2_sizes[0]),
            nn.ReLU(),
            nn.Conv1d(mlp2_sizes[0], mlp2_sizes[1], 1),
            nn.BatchNorm1d(mlp2_sizes[1]),
            nn.ReLU(),
            nn.Conv1d(mlp2_sizes[1], self.global_feature_size, 1),
            nn.BatchNorm1d(self.global_feature_size),
            nn.ReLU())

        if not device is None:
            self.device = device
            self.to(self.device)

    def send_to(self, device):
        self.device = device
        self.to(device)
        self.input_transformer.to(device)
        self.embedding_transformer.to(device)
        self.input_transformer.identity = self.input_transformer.identity.to(device)
        self.embedding_transformer.identity = self.embedding_transformer.identity.to(device)

    # Take as input a B x K x N matrix of B batches of N points with K
    # dimensions
    def forward(self,
                x,
                only_global_embedding=False,
                concat_transforms=False):

        print('x',x.shape) #5 x 3 x 600

        # Number of points put into the network
        N = x.shape[2]

        # First compute the input data transform and transform the data
        # T1 is B x K x K and x is B x K x N, so output is B x K x N
        T1 = self.input_transformer(x)
        x = torch.bmm(T1, x)

        # Run the transformed inputs through the first embedding MLP
        # Output is B x 64 x N
        x = self.mlp1(x)

        # Transform the embeddings. This gives us the "local embedding"
        # referred to in the paper/slides
        # T2 is B x 64 x 64 and x is B x 64 x N, so output is B x 64 x N
        T2 = self.embedding_transformer(x)
        local_embedding = torch.bmm(T2, x)

        # Further embed the "local embeddings"
        # Output is B x global_feature_size x N
        global_feature = self.mlp2(local_embedding)

        # Pool over the number of points. This results in the "global feature"
        # referred to in the paper/slides
        # Output should be B x global_feature_size x 1 --> B x global_feature_size (after squeeze)
        global_feature = F.max_pool1d(global_feature, N).squeeze(2)

        if concat_transforms:
            global_feature = torch.cat([
                global_feature,
                T1.view(-1, self.T1_reshape),
                T2.view(-1, self.T2_reshape)
            ])

        if only_global_embedding:
            return global_feature
        else:
            return global_feature, T1, T2






#
