import numpy as np, torch, os, sys
from torch import nn
from torch.autograd import Variable
from torch.nn import functional as F
import struct

from ..structures.TriangleMesh import TriangleMesh, FullTriangleMesh

def partial_chamfer_over_sets(U, V):
    '''
    Computes one term of the Chamfer distance between U and V.
    U and V are assumed to be batch-wise, i.e. U,V in batch_size x num_points_i x dim.
    They may have different num_points though.
    We compute:
        (1/|U_i|) sum_{u in U_i} min_{v in V_i} || u - v ||_2
    I.e. sum over the first argument, and min search over the second.
    Note U_i, V_i are single matrix valued entries in the order-3 "tensors" U and V.
    Output should be batch_size.
    '''
    # See: https://discuss.pytorch.org/t/efficient-distance-matrix-computation/9065/4
    B, nu, d = U.shape
    B, nv, d = V.shape
    ut = U.unsqueeze(2).expand(B,nu,nv,d)
    vt = V.unsqueeze(1).expand(B,nu,nv,d)
    return torch.pow(
                (ut - vt), 2
           ).sum(dim=3).min(dim=2)[0].sqrt().sum(dim=1) / nu
            # ).sum(dim=3).min(dim=2)[0].sqrt().sum(dim=1) / nu

def full_chamfer_over_sets(U, V, mean_over_batch=True):
    '''
    The first output takes the min over the first argument.
    So if U is the generated shape, then c1 is taking the point at min distance to each target in the reference shape, i.e. measuring the covering of the ref. Further, c2 would measure the min distance to the ref, for each point in the generated shape, i.e. forcing each point in the gen shape to be close to
    '''
    B, nu, d = U.shape
    B, nv, d = V.shape
    ut = U.unsqueeze(2).expand(B,nu,nv,d)
    vt = V.unsqueeze(1).expand(B,nu,nv,d)
    dists = torch.pow( (ut - vt), 2).sum(dim=3)
    # Mean over dim=1 first, to get mean over the target shape, then mean over dim=0, to get the mean over the batch. Hence, in total, just mean over everything.
    # c1 = dists.min(dim=1)[0].sqrt().mean()
    # c2 = dists.min(dim=2)[0].sqrt().mean()
    if mean_over_batch:
        c1 = dists.min(dim=1)[0].mean()
        c2 = dists.min(dim=2)[0].mean()
    else:
        c1 = dists.min(dim=1)[0].mean(dim=1)
        c2 = dists.min(dim=2)[0].mean(dim=1)
    return c1, c2


def full_chamfer_and_Hausdorff_over_sets(U, V, mean_over_batch=True):
    B, nu, d = U.shape
    B, nv, d = V.shape
    ut = U.unsqueeze(2).expand(B,nu,nv,d)
    vt = V.unsqueeze(1).expand(B,nu,nv,d)
    dists = torch.pow( (ut - vt), 2).sum(dim=3)
    init1 = dists.min(dim=1)[0] #.sqrt()
    init2 = dists.min(dim=2)[0] #.sqrt()
    if mean_over_batch:
        c1 = init1.mean()
        c2 = init2.mean()
        h1 = init1.max(dim=1)[0].mean()
        h2 = init2.max(dim=1)[0].mean()
    else:
        c1 = init1.mean(dim=1)
        c2 = init2.mean(dim=1)
        h1 = init1.max(dim=1)[0] #.mean(dim=1)
        h2 = init2.max(dim=1)[0] #.mean(dim=1)
    return c1, c2, h1, h2


def partial_chamfer_and_Hausdorff_over_sets(U, V):
    '''
    Computes one term of the Chamfer distance between U and V.
    U and V are assumed to be batch-wise, i.e. U,V in batch_size x num_points_i x dim.
    They may have different num_points though.
    We compute:
        (1/|U_i|) sum_{u in U_i} min_{v in V_i} || u - v ||_2
    I.e. sum over the first argument, and min search over the second.
    Note U_i, V_i are single matrix valued entries in the order-3 "tensors" U and V.
    Output should be batch_size.
    '''
    # See: https://discuss.pytorch.org/t/efficient-distance-matrix-computation/9065/4
    B, nu, d = U.shape
    B, nv, d = V.shape
    ut = U.unsqueeze(2).expand(B,nu,nv,d)
    vt = V.unsqueeze(1).expand(B,nu,nv,d)
    init = torch.pow( (ut - vt), 2 ).sum(dim=3).min(dim=2)[0] #.sqrt()
    chamfer_partial = init.sum(dim=1) / nu
    hauss_partial = init.max(dim=1)[0]
    return chamfer_partial, hauss_partial


################################################################################

##### Image loading #####

def load_mnist(containing_folder, set_type='train'):
    '''
    Load the classic MNIST dataset.
    containing_folder should contain the .idx*-ubyte files (unzipped) with their original names.
    Normalizes into [0,1]
    '''
    assert set_type in ['train', 'test'], "Must specify train or test for mnist"

    # Dataset type
    if set_type == 'train':
        img_file = 'train-images.idx3-ubyte'
        labels_file = 'train-labels.idx1-ubyte'
    else:
        img_file = 't10k-images.idx3-ubyte'
        labels_file = 't10k-labels.idx1-ubyte'

    # Index file reader
    # From: https://gist.github.com/tylerneylon/ce60e8a06e7506ac45788443f7269e40
    def read_idx(filename):
        filename = os.path.join(containing_folder, filename)
        with open(filename, 'rb') as f:
            zero, data_type, dims = struct.unpack('>HBB', f.read(4))
            shape = tuple(struct.unpack('>I', f.read(4))[0] for d in range(dims))
            return np.fromstring(f.read(), dtype=np.uint8).reshape(shape)

    return read_idx(img_file) / 255.0, read_idx(labels_file)


def convert_greyscale_image_to_mesh(img,
                                    normalize_xy=True,
                                    z_scale=1.0,
                                    threshold_and_crop=False,
                                    threshold=None,
                                    verbose=True,
                                    enforce_positive_z_normals=True):
    '''
    Converts a greyscale image to a triangle mesh.

    img: a numpy array in H x W
    normalize_xy: divides x & y by max{W,H} (preserves aspect ratio)
    '''
    H, W = img.shape
    V = np.zeros((H*W, 3))
    F = []
    xy_normer = H if H > W else W

    c = 0 # index into V
    for i in range(H):
        for j in range(W):
            # Set the current vertex
            x, y, z = j, -i, img[i,j] # Prevent flipping
            if normalize_xy: x, y = x / xy_normer, y / xy_normer
            V[c,:] = (x, y, z * z_scale)
            # Now set the triangles
            # Each vertex generates two triangle from "below" it, except the last row and column
            if (i == H-1) or (j == W-1):
                c += 1
                continue
            # Otherwise, use the fact that c = wi + j
            # Both are in CCW order
            F.append([c, c + 1,     c + W + 1])
            F.append([c, c + W + 1, c + W])
            # Increment current vertex
            c += 1

    # Generate a FullTriangleMesh
    print_info = verbose
    mesh = FullTriangleMesh(V, F, verbose=verbose, print_info=print_info)

    # Crop based on a value threshold (i.e. only keep values with a specific z-value or higher)
    if threshold_and_crop:
        assert not threshold is None, "Must specify a z-threshold!"
        # Gather nodes that meet the threshold
        targets = [ i for i,v in enumerate(mesh.vertices) if v[2] > threshold ]
        # Crop out the central mesh portion
        cut_mesh = mesh.generateSubmeshFromNodes(targets, verbose=verbose)
        # Only take the largest connected component
        mesh = cut_mesh.extractLargestConnectedComponent(verbose=verbose)

    # Enforce all the normals of the mesh to have positive z component
    if enforce_positive_z_normals:
        for i in range(mesh.num_vertices):
            vni = mesh.vertex_normals[i]
            if vni[2] < 0: mesh.vertex_normals[i] = -vni
        for j in range(mesh.num_faces):
            fni = mesh.face_normals[j]
            if fni[2] < 0: mesh.face_normals[j] = -fni

    return mesh



def count_parameters(model): # Trainable ones
    return sum(p.numel() for p in model.parameters() if p.requires_grad)



def StandardSequentialBNRL(indim, outdim, sizes, append_BN_NL=False):
    def std_triplet(isize, osize):
        return nn.Sequential(
                nn.Linear(isize, osize),
                nn.BatchNorm1d(osize),
                nn.ReLU()
        )
    # This is like this because ModuleList does not have forward...
    if (sizes is None) or (type(sizes) is list and len(sizes) == 0): # NO HIDDEN LAYERS
        if append_BN_NL:
            return std_triplet(indim, outdim)
        return nn.Linear(indim, outdim)
    elif type(sizes) is int: # ONE HIDDEN LAYER
        if append_BN_NL:
            return nn.Sequential(
                std_triplet(indim, sizes),
                std_triplet(sizes, outdim)
            )
        return nn.Sequential(
            std_triplet(indim, sizes),
            nn.Linear(sizes, outdim),
        )
    else:  # MULTIPLE HIDDEN LAYERS
        #assert len(sizes) <= 3, "Can add at most 3 layers"
        if len(sizes) == 1:
            return StandardSequentialBNRL(indim, outdim, sizes[0], append_BN_NL=append_BN_NL)
        S = [ indim ] + list(sizes) + [ outdim ]
        if append_BN_NL:
            layers = [ std_triplet(S[i-1], S[i]) for i in range(1, len(S)) ]
        else:
            layers = [ std_triplet(S[i-1], S[i]) for i in range(1, len(S)-1) 
                     ] + [ nn.Linear(S[-2], S[-1]) ]
        return nn.Sequential(*layers)

        #if len(sizes) == 2:
        #    if append_BN_NL:
        #        return nn.Sequential(
        #            std_triplet(indim, sizes[0]),
        #            std_triplet(sizes[0], sizes[1]),
        #            std_triplet(sizes[1], outdim)
        #        )
        #    return nn.Sequential(
        #        std_triplet(indim, sizes[0]),
        #        std_triplet(sizes[0], sizes[1]),
        #        nn.Linear(sizes[1], outdim)
        #    )
        #if len(sizes) == 3:
        #    if append_BN_NL:
        #        return nn.Sequential(
        #            std_triplet(indim, sizes[0]),
        #            std_triplet(sizes[0], sizes[1]),
        #            std_triplet(sizes[1], sizes[2]),
        #            std_triplet(sizes[2], outdim)
        #        )
        #    return nn.Sequential(
        #        std_triplet(indim, sizes[0]),
        #        std_triplet(sizes[0], sizes[1]),
        #        std_triplet(sizes[1], sizes[2]),
        #        nn.Linear(sizes[2], outdim)
        #    )






#
