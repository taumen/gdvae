import numpy as np
import numpy.linalg as LA
import numpy.random as npr

def lineUpAlongAxis(meshes, axis, additional_space=None, center_with_median=False,
                    center_with_bb_cen=False, center_by_minmax_alignment=False):
    '''
    Translates a list of meshes to line them up in the input axis.
    Should also work on PointCloud objects.
    axis can be in [0,1,2] or ['x','y','z']
    center_by_minmax_alignment must be a string like [x,y,z]_[min,max]
    '''
    assert not (center_with_bb_cen and center_with_median), "Choose one centering method"
    axes_map = {'x' : 0, 'y' : 1, 'z' : 2}
    if center_by_minmax_alignment != False:
        options = [ 'x_min', 'x_max', 'y_min', 'y_max', 'z_min', 'z_max' ]
        amsg = 'Min/Max alignment must be [x,y,z]_[min,max] format'
        assert center_by_minmax_alignment in options, amsg
        minMaxAligning = True
        alignment_axis = axes_map[ center_by_minmax_alignment[0] ] # numeric
        aligning_min = ( center_by_minmax_alignment.endswith('min') )
    else:
        minMaxAligning = False
    if type(axis) is str:
        axis = axes_map[axis]
    n = len(meshes)
    if additional_space is None: additional_space = 0.0
    # Center everything at the origin
    if minMaxAligning:
        # Move each shape so its min or max for the given axis is now at zero
        # Then when we slide it in a different dimension, the extremas remain aligned
        for s in meshes: 
            s.centerAt([0.0, 0.0, 0.0], 
                       use_bb_center=center_with_bb_cen,
                       use_median=center_with_median)
            translator = np.zeros(3)
            if aligning_min:
                value_at_extrema = s.axis_min( alignment_axis )
            else:
                value_at_extrema = s.axis_max( alignment_axis )
            translator[ alignment_axis ] = - value_at_extrema
            s.translate( translator ) # in place
    else:
        for s in meshes: s.centerAt([0.0, 0.0, 0.0], 
                                use_bb_center=center_with_bb_cen,
                                use_median=center_with_median)
    # Loop over and place each mesh
    for i in range(1, n):
        # Assume s1 has already been set in place.
        # Place s2 relative to it.
        s1, s2 = meshes[i-1], meshes[i]
        d1, d2 = s1.bounding_box_lengths()[axis], s2.bounding_box_lengths()[axis]
        blen =  (d1 / 2) + (d2 / 2) + additional_space
        s2.centerAt(s1.meanNodePosition())
        tvec_curr = np.zeros(3)
        tvec_curr[axis] = blen
        s2.translate( tvec_curr )
    # Recenter all meshes
    min_all = meshes[0].axis_min(axis)
    max_all = meshes[-1].axis_max(axis)
    t_len = max_all - min_all
    tvec = t_len - min_all # since the first model does not have xmin at zero
    t = np.zeros(3)
    t[axis] = -tvec
    for m in meshes: m.translate( t )

def lineUpInX(meshes, additional_space=None):
    '''
    Translates a list of meshes to line them up in the x-axis.
    Should also work on PointCloud objects.
    '''
    n = len(meshes)
    if additional_space is None: additional_space = 0.0
    # Center everything at the origin
    for s in meshes: s.centerAt([0.0, 0.0, 0.0])
    # Loop over and place each mesh
    for i in range(1, n):
        # Assume s1 has already been set in place.
        # Place s2 relative to it.
        s1, s2 = meshes[i-1], meshes[i]
        bx1, bx2 = s1.bounding_box_lengths()[0], s2.bounding_box_lengths()[0]
        blen =  (bx1 / 2) + (bx2 / 2) + additional_space
        s2.centerAt(s1.meanNodePosition())
        s2.translate([blen, 0, 0])
    # Recenter all meshes
    xmin_all = meshes[0].min_x()
    xmax_all = meshes[-1].max_x()
    x_len = xmax_all - xmin_all
    tvec = x_len - xmin_all # since the first model does not have xmin at zero
    for m in meshes:
        m.translate([-tvec, 0, 0])

def lineUpInY(meshes, additional_space=None):
    '''
    Translates a list of meshes to line them up in the y-axis.
    Should also work on PointCloud objects.
    '''
    lineUpAlongAxis(meshes, axis=1, additional_space=additional_space)

def lineUpInZ(meshes, additional_space=None):
    '''
    Translates a list of meshes to line them up in the y-axis.
    Should also work on PointCloud objects.
    '''
    lineUpAlongAxis(meshes, axis=2, additional_space=additional_space)





#
