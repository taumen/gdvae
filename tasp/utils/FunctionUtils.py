import numpy as np 
import numpy.linalg as LA 
import numpy.random as npr 

def sloped_step_function_asVector(start_val, end_val, slope_start, slope_end, num_values):
    '''
    Generates a vector representing a uniform sampling of a sloped step function.
    I.e.            / start_val for t < slope_start  
            f(t) = {  linear_interpolate(start_val, end_val) for slope_start <= t <= slope_end
                    \ end_val for t > slope_end
    where t in [0,1] over the vector.
    '''
    assert (0.0 < slope_start < 1.0) and (0.0 < slope_end < 1.0), "Slope values must be in (0,1)"
    assert slope_start < slope_end, "Require slope_start < slope_end"
    c1, c2 = int(slope_start * num_values), int(slope_end * num_values)
    n_bet = num_values - c1 - (num_values - c2)
    in_between = np.linspace(start_val, end_val, num=n_bet)
    step = start_val * np.ones(num_values)
    step[c1:c2] = in_between
    step[c2:] = end_val
    return step 

