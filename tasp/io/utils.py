import os, sys, json, base64
import numpy as np

### Reading and Writing JSON ###

def write_dict_to_json(data_dict, json_output_file, indent=4, sort_keys=True,
                        use_numpy_encoder=False):
    '''
    Write a Python object (dict or list of dicts) to a json file.

    TODO: the numpy encoder does not seem to be working.
    '''
    with open(json_output_file, 'w') as fp:
        if use_numpy_encoder:
            json.dump(data_dict, fp, sort_keys = sort_keys, indent = indent,
                                     cls = NumpyEncoder)
        else:
            json.dump(data_dict, fp, sort_keys = sort_keys, indent = indent)

def read_json(file, use_numpy_hook=False):
    '''
    Read in a JSON file as a Python object.

    TODO: the numpy hook may not be working.
    '''
    with open(file, 'r') as fp:
        if use_numpy_hook:
            data = json.load(fp, object_hook=json_numpy_obj_hook)
        else:
            data = json.load(fp)
    return data

class NumpyEncoder(json.JSONEncoder):
    '''
    From:
    https://stackoverflow.com/questions/3488934/simplejson-and-numpy-array/24375113
    '''

    def default(self, obj):
        """If input object is an ndarray it will be converted into a dict
        holding dtype, shape and the data, base64 encoded.
        """
        if isinstance(obj, np.ndarray):
            if obj.flags['C_CONTIGUOUS']:
                obj_data = obj.data
            else:
                cont_obj = np.ascontiguousarray(obj)
                assert(cont_obj.flags['C_CONTIGUOUS'])
                obj_data = cont_obj.data
            data_b64 = base64.b64encode(obj_data)
            return dict(__ndarray__=data_b64,
                        dtype=str(obj.dtype),
                        shape=obj.shape)
        # Let the base class default method raise the TypeError
        super(NumpyEncoder, self).default(obj)

def json_numpy_obj_hook(dct):
    '''
    Decodes a previously encoded numpy ndarray with proper shape and dtype.

    :param dct: (dict) json encoded ndarray
    :return: (ndarray) if input was an encoded ndarray
    '''
    if isinstance(dct, dict) and '__ndarray__' in dct:
        data = base64.b64decode(dct['__ndarray__'])
        return np.frombuffer(data, dct['dtype']).reshape(dct['shape'])
    return dct

### Reading and Writing Numpy Binary ###

def write_numpy(object_to_write, filename):
    '''
    Writes out a numpy binary file holding some python object.

    Note that this is a very generic method.
    For instance, it can write dicts of dicts with numpy arrays.

    Args:
        object_to_write: python object to write out
        filename: path to the file to write
    '''
    np.save(filename, object_to_write)

def read_numpy_binary(file_to_read, as_py2=False):
    '''
    Reads in a numpy binary file.

    Args:
        file_to_read: path to the file to read
        as_py2: treats it as though it were saved from python 2.
    '''
    if as_py2:
        return np.load(file_to_read, fix_imports=True, encoding='latin1').item()
    else:
        return np.load(file_to_read).item()







#
