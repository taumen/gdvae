# TTAA

import numpy as np
from plyfile import PlyData, PlyElement
import os, sys


def readOffFile(filepath):
    '''
    Input: Path to .off file
    Output: [node positions, triangles (list of triplets of node indices)]
    '''
    assert filepath.endswith(".off"), "Does not appear to be an off file"
    with open(filepath,"r") as q:
        lines = [ k.strip() for k in q.readlines() ] 
    numVerts, numFaces = [ int(y) for y in lines[1].split()[0:2] ]
    startLine = 2
    vertLines = [ a for a in lines[startLine:numVerts+startLine] if not a == '' ]
    faceLines = [ a for a in lines[numVerts+startLine:] if not a == '' ]
    assert len(vertLines) == numVerts, "Vertex number length mismatch "+str(len(vertLines))+" vs "+str(numVerts)
    assert len(faceLines) == numFaces, "Face number length mismatch "+str(len(faceLines))+" vs "+str(numFaces)
    v = np.array( [ np.array([float(s) for s in line.split()]) for line in vertLines ] )
    f = [ [int(u) for u in line.split()[1:]] for line in faceLines ]
    return [v, f]


def readTrianglesPlusVerticesMeshFiles(tri,vert):
    '''
    Input: Paths to triangles file and vertices file
    Output: [node positions, triangles (list of triplets of node indices)]
    '''
    nodes, triangles = None, None
    with open(tri,"r") as t:
        ts = t.readlines()
        # Subtract 1 to get indices from 0
        q = [ [int(a)-1 for a in p.strip().split()] for p in ts ]
        triangles = q
    with open(vert,"r") as v:
        vs = v.readlines()
        q = [ [float(a) for a in p.strip().split()] for p in vs ]
        nodes = np.array([np.array(r) for r in q])
    return [nodes, triangles]


def readPlyFile(filepath,verbose=False):
    '''
    Input: Path to .ply file
    Output: [node positions, triangles (list of triplets of node indices)]
    '''
    assert filepath.endswith('ply'), "Does not appear to be a ply file"
    if verbose: print("Reading file " + filepath)
    with open(filepath, 'rb') as f:
        plydata = PlyData.read(f)
    if verbose: print("\tConverting vertices")
    vertsr = plydata['vertex']        
    nverts = vertsr.count
    v = np.zeros(shape=(nverts,3))
    for i in range(0,nverts): 
        for j in range(0,3):
            v[i,j] = vertsr[i][j]
    facesr = plydata['face']
    nfaces = facesr.count
    f = np.zeros(shape=(nfaces,3),dtype=np.int32)
    if verbose: print("\tConverting faces")
    if isinstance(facesr[0][0],np.ndarray): k = 0
    else: k = 1 
    for i in range(0,nfaces): 
        f[i] = list( facesr[i][ k ] ) #[ int(ttt) for ttt in list( facesr[i][ k ] )]
    return [v,f]


def readObjFile(filepath, verbose=False, ignore_vertex_normals=False):
    '''
    Input: Path to .obj file
    Output: [node positions, triangles (list of triplets of node indices)]
    See: https://en.wikipedia.org/wiki/Wavefront_.obj_file
    '''
    if verbose: print('Reading', filepath)
    assert filepath.endswith('.obj'), 'Does not appear to be an obj file'
    with open(filepath,'r') as fhandle:
        lines = fhandle.readlines()
        lines = [line.strip() for line in lines if not line.strip() == '']
        if not 'OBJ' in lines[0]:
            if verbose: print('Warning: no OBJ header present in file', filepath)
    v, f = [], []
    vn = []
    for line in lines:
        # Comment lines
        if line.startswith('#'): continue
        # Vertex normals
        if line.startswith('vn'):
            vncurr = np.array(
                [ float(s) for s in line.split()[1:] ]
            )
            vn.append(vncurr)
        # Vertex lines
        elif line.startswith('v'):
            vcurr = np.array(
                [ float(s) for s in line.split()[1:] ]
            )
            v.append(vcurr)
        # Face/triangle lines
        elif line.startswith('f'):
            sline = line.split()[1:]
            if '/' in line:
                sline = [ item.split('/')[0] for item in sline ] # Ignore texture/normal coords
            fcurr = np.array(
                [ int(k) - 1 for k in sline ],  # Offset from 1
                dtype=np.int32
            )
            f.append(fcurr)
        else:
            if verbose: print('Warning: unparsable line', line)
    v, f = np.array(v), np.array(f)
    if len(vn) == len(v) and not ignore_vertex_normals:
        vn = np.array(vn)
        # Note: if vn is present, we presume that v & vn are in correspondence
        if verbose: print('Vertex normals present. Returning [v, f, vn]')
        return [v, f, vn]
    if len(vn) > 0: 
        if ignore_vertex_normals:
            if verbose: print('Vertex Normals present. Ignoring.')
        print('Warning: vertex normals detected, but not present for all nodes. Skipping.')
    if verbose: print('Returning [v, f]')
    return [v, f, None]

