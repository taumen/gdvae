import numpy as np
from scipy.sparse import lil_matrix

class DualGraph(object):
    '''
    A graph object representing the dual graph of a discrete input mesh.

    Attributes:
        dual_nodes: vertices of dual mesh (face centers from original). 
            Dims: |dual_V| x 3 = |F| x 3.
        dual_vertex_normals: each dual node has an associated face normal from the original mesh.
            Dims: |dual_V| x 3.
        dual_edges: edges of dual mesh (connections between adjacent faces from original).
            Each entry is two indices into dual_nodes.
            Dims: |dual_E| x 2 = 2 * num_shared_edges_in_original.
        dual_adjacency_matrix: whether or not 2 dual vertices are connected.
            M[i,j] = 1 if dual vertices i and j are connected.
            Dims: |dual_V| x |dual_V| = |F| x |F|.
        orig_c: original vertices concatenated in original order.
            So orig_c[f,i] is the ith node of face (or dual vertex) f, as a 3D vector.
            Dims: |dual_V| x 3 x 3 = |F| x 3 x 3.
    '''
    def __init__(self, mesh, consistency_check=True, verbose=True):
        '''

        Note: the dual edges do not capture boundary edges.
        
        Inputs: 
            mesh object: can be TriangleMesh for example
        '''
        if verbose: print('\nBuilding dual graph')
        V, F, E = mesh.vertices, mesh.faces, mesh.edges
        self.__NV, self.__NF, self.__NE = len(V), len(F), len(E)
        fnebs = lambda i: mesh.get_adjacent_faces_about_face(i) # face neighbours

        ### Dual nodes ###
        if verbose: print('Storing dual nodes')
        self.dual_nodes = np.array( mesh.face_centers, copy=True )

        ### Dual vertex normals ###
        if verbose: print('Storing dual vertex normals')
        self.dual_vertex_normals = np.array( mesh.face_normals, copy=True )

        ### Dual edges ###
        if verbose: print('Discerning dual edges')
        self.dual_edges = np.array([ np.array(e, copy=True) 
                                     for e in mesh.edge2face_map 
                                     if len(e) == 2 ])
        
        ### Dual adjacency matrix ###
        if verbose: print('Building dual adjacency')
        self.dual_adjacency_matrix = lil_matrix( (self.__NF, self.__NF), dtype=np.int8 )
        for i in range(self.__NF):
            for j in fnebs(i):
                self.dual_adjacency_matrix[i,j] = 1
                self.dual_adjacency_matrix[j,i] = 1

        ### Original Vertices per Dual Node ###
        if verbose: print('Storing concatenated vertices')
        self.orig_c = np.array(V[F], copy=True)

        if consistency_check:
            if verbose: print('Checking consistency')
            assert self.__NF == len(self.dual_vertex_normals)
            assert self.dual_adjacency_matrix.sum() // 2 == len(self.dual_edges)
            
    def display_info(self):
        print('-- Dual graph info --')
        print('Dual vertices shape',       self.dual_nodes.shape)
        print('Dual vertex normals shape', self.dual_vertex_normals.shape)
        print('Dual edges shape',          self.dual_edges.shape)
        print('Dual adjacency shape',      self.dual_adjacency_matrix.shape)
        print('Dual corner points shape',  self.orig_c.shape)
        print('Original sizes: |V|=%d, |E|=%d, |F|=%d' % (self.__NV, self.__NE, self.__NF))




#
