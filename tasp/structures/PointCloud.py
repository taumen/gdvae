
import numpy as np, os, sys, sklearn
from sklearn.neighbors import KDTree, NearestNeighbors
from sklearn.neighbors import kneighbors_graph as knn_graph
from sklearn.metrics.pairwise import rbf_kernel as pairwise_rbf
from scipy.sparse.csgraph import laplacian as csgraph_laplacian
from scipy.sparse.linalg import eigsh
from scipy.sparse import csc_matrix as cscm
from scipy.sparse import diags as ssdiags

from ..utils.TransformUtils import *
from ..viewing.Color import Color
from ..utils.ViewUtils import lineUpAlongAxis

class PointCloud:

    def __init__(self, points, copy=True, precomputed_kdtree=None,
                 known_normals=None, colours=None, compute_kdt=False, 
                 radii=None):
        if copy: self.points = np.copy(points)
        else:    self.points = np.array(points)

        if precomputed_kdtree is None:
            if compute_kdt:
                self.node_tree = KDTree(self.points)
            else:
                self.node_tree = None
        else:
            if type(precomputed_kdtree) is KDTree: self.node_tree = precomputed_kdtree
            else: print('Precomputed KDtree is not a KDtree')

        if colours is None:
            self.point_colors = [ Color() for _ in self.points ]
        else:
            assert len(colours) == len(self.points), "Untenable length of colours"
            self.point_colors = colours

        if not known_normals is None: self.assignNormals(known_normals)

        self.radii = radii

    def pc_laplacian(self,
                     lap_type='graph',
                     hk_sigma_bandwidth='default',
                     num_nn=None,
                     normalize_L=False,
                     store=True):
        '''
        Compute the Laplacian operator matrix of the point cloud.

        Args:
            lap_type (str): 'hk' to use Gaussian heat kernel function (RBF) to
                transform distances to similarities; 'graph' to use KNN as the
                affinity matrix; 'diffusion' to follow the laplacian eigenmaps approach.
            hk_sigma_bandwidth (float): size of the std dev of the smoothing
                kernel used to compute weighted adjacency similarities. Only
                used if `lap_type == 'hk'`. 
            num_nn (int): number of nearest neighbours to use to generate a graph
            store (bool): whether to store L in the object

        Returns:
            the point cloud Laplacian L
        '''
        assert lap_type in ['hk', 'graph', 'diffusion']

        def default_t():
            # t ~ (1/4) * epsilon^( -1 / (2 + xi) )
            nn_dists = self.dists_to_nearest_neighbours()
            approx_eps = np.mean(nn_dists) # or max? # ~epsilon
            _xi_ = 0.01
            pwr_exponent = -1.0 / (2 + _xi_)
            hk_sigma_bandwidth = 0.25 * np.power(approx_eps, pwr_exponent)
            return hk_sigma_bandwidth

        if lap_type == 'hk':
            X = self.points
            # Note: use knn_graph with mode='distance'
            #dists = knn_graph(X=self.points, n_neighbors=num_nn,
            #            mode='distance', metric='minkowski', p=2)
            #sims = np.exp(-hk_sigma_bandwidth * dists ** 2)
            assert type(hk_sigma_bandwidth) in [str, float]
            # Note Belkin's "Constructing Laplace Operator from Point Clouds in
            # R^d" discusses how to choose "bandwidths" for epsilon-sampled
            # manifolds a bit.
            if hk_sigma_bandwidth == 'default':
                hk_sigma_bandwidth = default_t()
            # Weighted adjacency matrix based on Gaussian heat kernel similarities
            # RBF kernel: K(x, y) = exp(-gamma ||x-y||^2)
            A = sklearn.metrics.pairwise.rbf_kernel(X, Y=X, gamma=hk_sigma_bandwidth)
            A = A / (hk_sigma_bandwidth * np.pi)
        elif lap_type == 'diffusion':
            # Follow the procedure in Laplacian-Eigenmaps
            # Note that t = hk_sigma_bandwidth, but only k-NNs are used
            X = self.points
            # t value
            if hk_sigma_bandwidth == 'default':
                hk_sigma_bandwidth = default_t()
            # Diffusion pairwise distance
            A = sklearn.metrics.pairwise.rbf_kernel(X, Y=X, gamma=hk_sigma_bandwidth)
            # Nearest neighbours
            knng = knn_graph(X=self.points, n_neighbors=num_nn,
                    mode='connectivity', metric='minkowski', p=2)
            adj = (knng + knng.T) / 2.0
            # Diffusion similarities matrix with far entries zerod out
            W = A * adj # zeroed similarities matrix
            # Compute pseudo-areas
            D = np.diag( W.sum(axis=1) )
            # Compute Laplacian
            L = D - W
            if store:
                self.L = L
                self.pseudo_areas = np.diag(D) # As a vector now
            return L
        elif lap_type == 'graph':
            knng = knn_graph(X=self.points, n_neighbors=num_nn,
                    mode='connectivity', metric='minkowski', p=2)
            A = (knng + knng.T) / 2.0
        # Compute Laplacian from Adjacency
        L, Ldiag = csgraph_laplacian(A, normed=normalize_L, return_diag=True)
        # Ldiag = np.power(Ldiag, -1)
        if store:
            self.L = L
            self.pseudo_areas = Ldiag # For graphs, contains vertex degrees
        return L

    def compute_lbo_spectrum(self, num_eigvals,
                             use_pseudo_areas=True,
                             lap_type='hk',
                             normalize_L=False,
                             hk_sigma_bandwidth=None, 
                             num_nn=None, 
                             store=True):
        '''
        Computes a PCLBO spectrum of this shape.
        '''
        if hasattr(self, 'L'):
            L = self.L
        else:
            L = self.pc_laplacian(lap_type=lap_type,
                                  hk_sigma_bandwidth=hk_sigma_bandwidth,
                                  normalize_L=normalize_L,
                                  num_nn=num_nn,
                                  store=store)
        if use_pseudo_areas:
            S = cscm( ssdiags( self.pseudo_areas, offsets=0 ) )
        else:
            S = None
        # Note: consider whether it's better to use M as pseudo areas with unnormalized Laplacian?
        Lambda, Phi = eigsh(
            L, # Sparse weight matrix
            k = num_eigvals, # Number of desired eigenvalues/eigenvectors
            M = S, # The generalized coefficient matrix for the eigenvalue problem
            sigma = 0, # Get eigenvalues near zero, using shift-invert mode
            which = 'LM'#, # Return the eigenvalues of smallest magnitude (in shift-invert mode)
            #maxiter = max_iters,
            #tol = tol
        )
        ### Return results
        Lambda, Phi = np.sort(Lambda), np.fliplr(Phi)
        if store:
            self.Lambda = Lambda
            self.Phi = Phi
        return self.Lambda, self.Phi

    def random_subsample(self, n_points):
        """ Return subsample of points """
        if n_points >= self.num_points():
            return np.copy(self.points)
        return np.random.choice(self.points, n_points, replace=False)

    def random_subsample_obj(self, n_points):
        if n_points > self.num_points():
            raise ValueError('Asking to subsample to too many points')
        total_points = self.num_points()
        inds = np.random.choice(total_points, n_points, replace=False)
        # Points
        P = self.points[inds, :]
        # Radii
        r = None if self.radii is None else np.array(self.radii)
        if not self.radii is None:
            r = r[inds]
        # Colours
        if not self.point_colors is None:
            C = [ self.point_colors[i] for i in inds ]
        # Final new object
        return PointCloud(P, radii=r, colours=C)

    def rotate(self, R, in_place=True):
        R = np.array(R)
        if in_place:
            self.points = self.points.dot(R)
            self.node_tree = None # Equivalent to coordinate system changing
            return self
        else:
            return PointCloud(self.points.dot(R), colours=None)

    def translate(self, v, in_place=True):
        v = np.array(v)
        if in_place:
            self.points += v
            # pairwise dists and neighbors are preserved but given an input point
            # it will not be in the same coord system as when the tree was made
            self.node_tree = None
            return self
        else:
            return PointCloud(self.points + v, colours=None)

    def centerAt(self, newCenter, in_place=True, use_median=False, use_bb_center=False):
        nc = np.array(newCenter)
        if use_median:
            currMean = np.median(self.points, axis=0)
        elif use_bb_center:
            currMean = self.bounding_box_center()
        else:
            currMean = np.mean(self.points, axis=0)
        delta = nc - currMean
        if in_place:
            self.points += delta
            self.node_tree = None
        else:
            return PointCloud(self.points + delta, colours=None)

    def center_at_origin_via_bb(self, in_place=True):
        self.centerAt( np.array([0.0,0.0,0.0]), in_place=in_place, use_bb_center=True )

    def bounding_coordinates(self):
        # Format: [min_x, max_x, min_y, max_y, min_z, max_z]
        return [ self.min_x(), self.max_x(),
                 self.min_y(), self.max_y(),
                 self.min_z(), self.max_z() ]

    def bounding_box_center(self):
        b = self.bounding_coordinates()
        return np.array([ (b[0]+b[1])/2, (b[2]+b[3])/2, (b[4]+b[5])/2 ])

    def min_x(self): return np.min(self.points[:,0], axis=0)
    def max_x(self): return np.max(self.points[:,0], axis=0)
    def min_y(self): return np.min(self.points[:,1], axis=0)
    def max_y(self): return np.max(self.points[:,1], axis=0)
    def min_z(self): return np.min(self.points[:,2], axis=0)
    def max_z(self): return np.max(self.points[:,2], axis=0)

    def axis_min(self, i): return np.min(self.points[:,i], axis=0)
    def axis_max(self, i): return np.max(self.points[:,i], axis=0)

    def bounding_box_lengths(self):
        bs = self.bounding_coordinates()
        return np.array([bs[1] - bs[0], bs[3] - bs[2], bs[5] - bs[4]])

    def meanNodePosition(self):
        return np.mean(self.points, axis=0)

    def num_points(self):
        return len(self.points)

    def dists_to_nearest_neighbours(self, store_helper=True):
        # Computing minimum and average distances
        X = self.points
        nbrs = NearestNeighbors(n_neighbors=2).fit(X)
        if store_helper:
            self._nn_struct = nbrs
        distances, indices = nbrs.kneighbors(X)
        distances = distances[:,1] # ignore first neighbour (it's the point itself, so 0)
        return distances

    def combine(point_clouds,
                line_up=True,
                carry_colours=False,
                line_up_axis=0,
                align_with_median=False,
                align_with_bb_cen=False,
                align_min_or_max_dim=False,
                compute_kdt=True, # Usually not needed if just visualizing
                carry_radii=True
        ):
        '''
        Input: list of PointCloud objects
        Output: a single PointCloud object

        align_min_or_max_dim must be a string [x,y,z]_[min,max]

        A trivial way to create a grid is to
            (1) combine several sets along the x axis
            (2) combine each set along the y axis together
        '''
        amsg_type = 'Only specify one alignment type'
        if align_min_or_max_dim is None:
            align_min_or_max_dim = False
        if align_min_or_max_dim != False:
            align_min_or_max_dim = align_min_or_max_dim.lower()
            options = [ 'x_min', 'x_max', 'y_min', 'y_max', 'z_min', 'z_max' ]
            assert align_min_or_max_dim in options, 'Min/Max alignment must be [x,y,z]_[min,max]'
            usingMinMaxAlignment = True
        assert not (align_with_median and align_with_bb_cen), amsg_type
        #assert not (align_with_median and usingMinMaxAlignment), amsg_type
        #assert not (align_with_bb_cen and usingMinMaxAlignment), amsg_type

        if line_up:
            lineUpAlongAxis(point_clouds,
                            axis=line_up_axis,
                            center_with_median=align_with_median,
                            center_with_bb_cen=align_with_bb_cen,
                            center_by_minmax_alignment=align_min_or_max_dim,
                            additional_space=0.2)
        ns = [ pc.num_points() for pc in point_clouds ]
        new_ps = np.zeros((sum(ns), 3))
        current_position = 0
        for (pc, n) in zip(point_clouds, ns):
            new_ps[current_position:current_position+n, :] = pc.points
            current_position += n

        if carry_radii:
            nrs = []
            for pc in point_clouds:
                pc.set_radius()
                n_points = pc.num_points()
                nrs += ( [ pc.median_radius ] * n_points )
        else:
            nrs = None

        if carry_colours:
            ncs = []
            for pc in point_clouds:
                for c in pc.point_colors:
                    ncs.append(c)
            return PointCloud(new_ps, colours=ncs, compute_kdt=compute_kdt, radii=nrs)
        else:
            return PointCloud(new_ps, compute_kdt=compute_kdt, radii=nrs)


    def grid_combine(rows, carry_colours=True, h_padding=0.01, v_padding=0.01,
                     align_y_mins=False, carry_radii=True):
        '''
        Input: list of lists of PointCloud objects
        Output: a single PointCloud object

        Each row is lined up in x, and then stacked in y.
        Each column needs enough room for the largest bounding box.
        '''
        # Checks
        for r in rows: assert len(r) == len(rows[0]), "Rows must be the same length"
        #--- Precomputations ---#
        NUM_ROWS, NUM_COLS = len(rows), len(rows[0])
        N_total_per_row = [ sum([pc.num_points() for pc in r]) for r in rows ]
        N_total_points = sum(N_total_per_row)
        new_point_cloud = np.zeros((N_total_points, 3))
        # Compute the width of each column (as the max over the column)
        widths = [ [ pc.bounding_box_lengths()[0] for pc in r ] for r in rows ]
        column_widths = [ ]
        for j in range(NUM_COLS):
            max_width = 0
            for ri, r in enumerate(rows):
                curr_width = widths[ri][j]
                if curr_width > max_width: max_width = curr_width
            column_widths.append(max_width)
        # Compute the height of each row (as the max over the row)
        heights = np.array([ [ pc.bounding_box_lengths()[1] for pc in r ] for r in rows ])
        row_heights = np.max( heights, axis = 1 )
        # XY centers --> XY_centers[i,j] = row_i,col_j center = y, x
        XY_centers = np.zeros((NUM_ROWS, NUM_COLS, 3))
        for i in range(NUM_ROWS):
            current_centers = []
            # Compute the y value for this row
            if i == 0: y = 0.0 # For the first row, the y value is zero
            else:
                prev_y = XY_centers[i-1,0,1]
                offset_y = -( (row_heights[i-1] + row_heights[i]) / 2.0 + v_padding )
                y = prev_y + offset_y
            XY_centers[i,:,1] = y
        # Compute the smallest height seen per row (for aligning y_mins)
        min_height_per_row = []
        for i in range(NUM_ROWS):
            min_height_per_row.append( min( heights[i] ) )
        # Compute the x centers for every element in the row
        # Note that the x values of each row are the same, across rows
        for j in range(NUM_COLS):
            if j == 0: x = 0.0
            else:
                prev_x = XY_centers[0,j-1,0]
                offset_x = (column_widths[j-1] + column_widths[j]) / 2.0 + h_padding
                x = prev_x + offset_x
            XY_centers[:,j,0] = x
       
        # We now have the desired centers of the bounding boxes of each target
        # We can then compute the vector to move each PC to its target spot
        current_index = 0
        ncs = []
        nrs = []
        for i in range(NUM_ROWS):
            for j in range(NUM_COLS):
                current_target_center = XY_centers[i,j]
                current_PC = rows[i][j]
                current_PC_BB_center = current_PC.bounding_box_center()
                transport_vector = current_target_center - current_PC_BB_center
                if align_y_mins:
                    # Alter the transport vector to align the min y values across the rows
                    min_height = min_height_per_row[i]
                    curr_target_height = heights[i,j]
                    delta_height_change = 0.5 * (curr_target_height - min_height)
                    transport_vector[1] = transport_vector[1] + delta_height_change
                # Now add this cloud to the new cloud
                for p in current_PC.points:
                    new_point_cloud[current_index,:] = p + transport_vector
                    current_index += 1
                if carry_colours:
                    for c in current_PC.point_colors:
                        ncs.append(c)
                if carry_radii:
                    current_PC.set_radius()
                    nrs += current_PC.radii
        if not carry_radii:
            nrs = None 
        if carry_colours:
            return PointCloud(new_point_cloud, colours=ncs, radii=nrs)
        else:
            return PointCloud(new_point_cloud, radii=nrs)

    def assignNormals(self, normals):
        self.normals = normals

    def makeRandomSphere(center_position, radius, num_points):
        # Sample points from unit sphere
        mu, sigma = 0, 1
        rps = np.random.normal(mu, sigma, size=(num_points, 3))
        for k in range(num_points):
            cnorm = np.linalg.norm( rps[k,:] )
            rps[k,:] /= cnorm
        # Scale to right radius
        rps *= radius
        # Translate to correct center position
        rps += center_position
        return PointCloud(rps, copy=False)

    def makeRandomCube(center_position, side_length, num_points, randomly_rotate=True):
        points = np.zeros((num_points, 3))
        sheet_choices = np.random.randint(low=0, high=6, size=num_points)
        random_points = np.random.uniform(low=-1.0, high=1.0, size=(num_points,2))
        for i in range(num_points):
            sheet_num = sheet_choices[i]
            p = random_points[i, :]
            # Choose sheet: x = +- 1, y = +- 1, z = +- 1
            if sheet_num == 0:
                points[i,0] = 1.0
                points[i,1] = p[0]
                points[i,2] = p[1]
            elif sheet_num == 1:
                points[i,0] = -1.0
                points[i,1] = p[0]
                points[i,2] = p[1]
            elif sheet_num == 2:
                points[i,0] = p[0]
                points[i,1] = 1.0
                points[i,2] = p[1]
            elif sheet_num == 3:
                points[i,0] = p[0]
                points[i,1] = -1.0
                points[i,2] = p[1]
            elif sheet_num == 4:
                points[i,0] = p[0]
                points[i,1] = p[1]
                points[i,2] = 1.0
            else: # sheet_num == 0:
                points[i,0] = p[0]
                points[i,1] = p[1]
                points[i,2] = -1.0
        # Scale cube to right side-length
        points *= side_length / 2.0
        # Perform random rotation, if needed
        if randomly_rotate:
            R = random3DRotationMatrix_unitary()
            points = np.dot(points, R)
        # Translate to desired center
        points += np.array(center_position)
        return PointCloud(points, copy=False)

    def plot3D_mpl(self):
        from matplotlib import pyplot
        import pylab
        from mpl_toolkits.mplot3d import Axes3D
        fig = pylab.figure()
        ax = Axes3D(fig)
        def split3D(ps):
            xs = [p[0] for p in ps]
            ys = [p[1] for p in ps]
            zs = [p[2] for p in ps]
            return [xs,ys,zs]
        xs,ys,zs = split3D(self.points)
        ax.scatter(xs,ys,zs)
        pyplot.show()

    def display_ogl_1(self, verbose=False):
        import trimesh
        #from trimesh import Trimesh
        from trimesh.points import PointCloud as tpc
        tmesh_pc = tpc( self.points )
        tmesh_pc.colors = [ p.as_normalized_rgb().tolist() + [0.99] for p in self.point_colors ]
        scene = tmesh_pc.scene()
        if verbose: print('Built scene')
        from ..viewing.SceneViewCustom import SceneViewer
        def viewer(): SceneViewer(scene, tasp_mesh=None, marked_points=None)
        block = True
        if block: viewer()
        else:
            from threading import Thread
            Thread(target=viewer).start()

    def display(self, verbose=False):
        self.display_ogl_1()

    def color_by_coords(self, coord_index, cfunc):
        assert coord_index in [0,1,2], "Untenable indices in colorer"
        cs = self.points[:,coord_index] # Coordinate function over the manifold
        # Normalization of coordinate values
        minc, maxc = np.min(cs), np.max(cs)
        cs = (cs - minc) / (maxc - minc)
        self.point_colors = [ Color(cfunc(c)) for c in cs]
        return self

    def simple_color_by_coords(self, coord_index, color_index, oth_value=0.5):
        if type(oth_value) is tuple:
            othval1 = oth_value[0]
            othval2 = oth_value[1]
        else:
            othval1 = oth_value
            othval2 = oth_value
        assert coord_index in [0,1,2] and color_index in [0,1,2], "Untenable indices in colorer"
        assert othval1 <= 1.0 and othval1 >= 0.0, "Untenable default rgb value (1)"
        assert othval2 <= 1.0 and othval2 >= 0.0, "Untenable default rgb value (2)"
        cs = self.points[:,coord_index]
        minc, maxc = np.min(cs), np.max(cs)
        cs = (cs - minc) / (maxc - minc)
        if color_index == 0:
            oth_value = np.array([0.0, othval1, othval2])
        elif color_index == 1:
            oth_value = np.array([othval1, 0.0, othval2])
        else:
            oth_value = np.array([othval1, othval2, 0.0])
        q = np.zeros((len(self.points), 3)) + oth_value
        q[:, color_index] = cs
        self.point_colors = [ Color(ccol) for ccol in q ]
        return self

    def simple_color(self, color_array):
        c = Color( np.array(color_array) )
        self.point_colors = [c for p in self.points]
        return self

    def set_radius(self, set_radii=True, m_factor=0.5):
        if hasattr(self, 'median_radius') and hasattr(self, 'mean_radius'):
            return
        dists = self.dists_to_nearest_neighbours()
        self.median_radius = np.median(dists)
        self.mean_radius = np.mean(dists)
        if set_radii:
            n_points = self.num_points()
            self.radii = [ self.median_radius * m_factor ] * n_points

    def writeToMitsubaFile(self, 
                      output_file, 
                      up_dir="0, 1, 0", 
                      default_target=(0,0,0),
                      ball_radius_alpha=0.91, 
                      camera_on_positive_z=True,
                      fixed_radius=None,
                      img_size=1000,
                      # Whether to use the 1-NN distance distribution to compute radius
                      use_neb_dists_for_radius=True, 
                      use_dynamic_stored_radii=True,
                      use_varia_colors=True, 
                      verbose=True, 
                      camera_lift=0.75,# 0 = y-origin, 1 = max_y
                      lookAt_lowering=0.45,# 0 = y-origin, 1 = min_y
                      overwrite_output=False, 
                      subsample_points=None, # or an int to subsample to
                      zero_center_via_BB=True):
        '''
        Generate a Mitsuba-format scene with this point cloud.

        Note this assumes zero-centering of this shape.
        '''
        if verbose: print('Starting conversion to Mitsuba scene')

        # TODO subsampling is bugged!
        subsample = ( type(subsample_points) is int )

        if zero_center_via_BB:
            self.center_at_origin_via_bb()

        # Fast-fail check for existent file
        if not overwrite_output:
            if os.path.exists(output_file):
                print(output_file, 'already exists!')
                sys.exit(0)

        IMAGE_SIZE = img_size    # Size of image in Mitsuba GUI
        if type(IMAGE_SIZE) is int:
            IMAGE_WIDTH  = IMAGE_SIZE
            IMAGE_HEIGHT = IMAGE_SIZE
        else:
            IMAGE_WIDTH  = IMAGE_SIZE[0]
            IMAGE_HEIGHT = IMAGE_SIZE[1]
        
        ##### Fixed Settings #####
        #--------------------------------------------------------------------------------#
        SAMPLE_COUNT = 64        
        FLOOR_OFFSET_MULT = 0.01 
        FLOOR_SCALE_MULT = 10.0  
        FOV = 45                 
        object_size_mult = 1.59  
        # Directional light direction
        LX = 0.0 #0.05
        LY = -1
        LZ = 0.05 # -0.79
        DIRECTIONAL_LIGHT_IRRADIANCE = 0.5 
        CONST_RADIANCE = 1.0      
        #--------------------------------------------------------------------------------#

        ##### Preparations #####
        # Camera and lighting
        if camera_on_positive_z:
            LZ *= -1
        CR = ", ".join( [str(CONST_RADIANCE)] * 3 )
        TARGET, UP_DIR = list(default_target), up_dir
        # Compute the bounding box
        min_x, max_x, min_y, max_y, min_z, max_z = self.bounding_coordinates()
        X_LEN, Y_LEN, Z_LEN = self.bounding_box_lengths()

        ##### Dynamic Settings #####
        DYNAMIC_RADIUS = False
        RADII = None
        # Manually specifying a fixed radius works 
        if not ( fixed_radius is None ):
            print('Using fixed pre-specified radii')
            SPHERE_RADIUS = fixed_radius
        # Main case: 
        if use_dynamic_stored_radii:
            print('Using dynamic stored radii')
            DYNAMIC_RADIUS = True
            if self.radii is None:
                self.set_radius()
            RADII = self.radii
        else:
            print('Computing singular radius')
            if not use_neb_dists_for_radius:
                # Computing minimum and average distances
                from scipy.spatial.distance import pdist
                dists = pdist(self.points, 'euclidean') # returns tri_upper of dist matrix as a 1d array
                MIN_DIST = np.min(dists)
                AVG_DIST = np.mean(dists)
                SPHERE_RADIUS = ball_radius_alpha * (MIN_DIST / 2) + (1 - ball_radius_alpha) * (AVG_DIST / 2)
            if use_neb_dists_for_radius:
                dists = self.dists_to_nearest_neighbours()
                #MIN_DIST = np.min(dists)
                #AVG_DIST = np.mean(dists)
                SPHERE_RADIUS = np.mean(dists) / 2 # np.median(dists) / 2
                #SPHERE_RADIUS = ball_radius_alpha * (MIN_DIST / 2) + (1 - ball_radius_alpha) * (AVG_DIST / 2)
        if verbose: print('Finished preparations')

        # We determine the distance the camera should be from the shape based on the size of the shape and
        # the FOV. 
        OBJ_SIZE = max(Y_LEN, X_LEN) * object_size_mult
        CAMERA_DISTANCE = (OBJ_SIZE / 2) / np.tan((FOV / 2) * (np.pi / 180.0))
        CAMERA_Y = camera_lift * max_y
        SD_SIGN = '' if camera_on_positive_z else '-'
        ORIGIN = "0, %f, %s%f" % (CAMERA_Y, SD_SIGN, CAMERA_DISTANCE)
        # Push down the floor a bit, so it's not touching the shape
        FLOOR_Y_OFFSET = FLOOR_OFFSET_MULT * Y_LEN
        # Expand the floor
        FLOOR_SCALE = max(Z_LEN, X_LEN) * FLOOR_SCALE_MULT
        # Point the camera down a bit
        TARGET[1] += lookAt_lowering * min_y
        TARGET = ", ".join([str(ttt) for ttt in TARGET])

        ##### Set the material properties of the spheres #####
        # Case 1: use the color properties of each point to enforce the material
        if use_varia_colors:
            def material_string(i):
                r, g, b = self.point_colors[i].as_normalized_rgb().tolist()
                matstr  = '\t\t<bsdf type="plastic">\n'
                matstr += '\t\t\t<rgb name="diffuseReflectance" value="%f, %f, %f"/>\n' % (r, g, b)
                matstr += '\t\t\t<float name="intIOR" value="1.9"/>\n'
                matstr += '\t\t</bsdf>\n'
                return matstr
        # Case 2: give each sphere the same material and color
        else:
            MAT_ID_NAME = "tasp_mat"
            def material_string(i):
                return '\t\t<ref id="%s"/>\n' % MAT_ID_NAME

        ##### Generate the XML String #####
        # Start of document
        if verbose: print('Starting xml generation')
        with open(output_file,'w') as fh:
            s  = '<?xml version="1.0" encoding="utf-8"?>\n'
            # Start of the scene (and integrator)
            s += '<scene version="0.5.0">\n\t<integrator type="path"/>\n'
            # Define the sensor (camera, sampler, image)
            s += ( '\t<sensor type="perspective">\n' +
                    '\t\t<transform name="toWorld">\n' +
                        '\t\t\t<lookAt origin="%s" target="%s" up="%s"/>\n' % (ORIGIN, TARGET, UP_DIR) +
                    '\t\t</transform>\n' +
                    '\t\t<float name="fov" value="%f"/>\n' % FOV +
                    '\t\t<sampler type="ldsampler">\n' +
                        '\t\t\t<integer name="sampleCount" value="%d"/>\n' % SAMPLE_COUNT +
                    '\t\t</sampler>\n' +
                    '\t\t<film type="hdrfilm">\n' +
                        '\t\t\t<integer name="width" value="%d"/>\n' % IMAGE_WIDTH +
                        '\t\t\t<integer name="height" value="%d"/>\n' % IMAGE_HEIGHT +
                        '\t\t\t<rfilter type="gaussian"/>\n' +
                    '\t\t</film>\n' +
                '\t</sensor>\n' )
            # Add emitters
            s += ( '\t<emitter type="directional">\n' +
                    '\t\t<vector name="direction" x="%f" y="%f" z="%f"/>\n' % (LX,LY,LZ) +
                    '\t<float name="samplingWeight" value="1"/>\n' +
                    '\t<spectrum name="irradiance" value="%f"/>\n' % DIRECTIONAL_LIGHT_IRRADIANCE +
                '\t</emitter>\n' +
                '\t<emitter type="constant">\n' +
                    '\t\t<float name="samplingWeight" value="1"/>\n' +
                    '\t\t<rgb name="radiance" value="%s"/>\n' % CR +
                '\t</emitter>\n' )
            # If not in varia mode, define a material to be used by every sphere
            if not use_varia_colors: # 
                s += '\t<bsdf type="plastic" id="%s">\n' % MAT_ID_NAME
                s += '\t\t<rgb name="diffuseReflectance" value="0.79, 0.1, 0.1"/>\n'
                s += '\t\t<float name="intIOR" value="1.9"/>\n\t</bsdf>\n'
            # Add floor plane
            s += (  '\t<shape type="rectangle">\n' +
                        '\t\t<bsdf type="diffuse">\n' +
                            '\t\t\t<rgb name="diffuseReflectance" value="0.99, 0.99, 0.99"/>\n' +
                        '\t\t</bsdf>\n' +
                        '\t\t<transform name="toWorld">\n' +
                            '\t\t\t<rotate x="1" angle="90" />\n' +
                            '\t\t\t<scale x="%f" y="1" z="%f"/>\n' % (FLOOR_SCALE, FLOOR_SCALE) +
                            '\t\t\t<translate y="%f"/>\n' % (min_y - FLOOR_Y_OFFSET) +
                        '\t\t</transform>\n' +
                        '\t\t<boolean name="flipNormals" value="true"/>\n' +
                    '\t</shape>\n' )
            fh.write(s)
            # Add spheres
            n = self.num_points()
            bools_to_keep = [ True ] * n
            if subsample:
                n_to_keep = subsample_points
                if n <= n_to_keep: 
                    subsample = False
                else:
                    to_keep = npr.choice(n, size=n_to_keep, replace=False)
                    for tk in to_keep:
                        bools_to_keep[tk] = False
            jk = sum([ (1 if b else 0) for b in bools_to_keep ])
            print('N_kept', jk)
            if DYNAMIC_RADIUS:
                for i in range(n):
                    if not bools_to_keep[i]: continue
                    px, py, pz = self.points[i, :]
                    CURR_RADIUS = RADII[i]
                    fh.write( '\t<shape type="sphere">\n' +
                                '\t\t<float name="radius" value="%f" />\n' % CURR_RADIUS +
                                '\t\t<point name="center" x="%f" y="%f" z="%f" />\n' % (px, py, pz) +
                                material_string(i) +
                           '\t</shape>\n' )
                    fh.flush()
            else:
                for i in range(n):
                    if not bools_to_keep[i]: continue
                    px, py, pz = self.points[i, :]
                    fh.write( '\t<shape type="sphere">\n' +
                                '\t\t<float name="radius" value="%f" />\n' % SPHERE_RADIUS +
                                '\t\t<point name="center" x="%f" y="%f" z="%f" />\n' % (px, py, pz) +
                                material_string(i) +
                           '\t</shape>\n' )
                    fh.flush()
            
            # Final end to the scene
            fh.write('</scene>')

            # Now write everything to the target file
            if verbose: print('Wrote scene to', output_file)




#
