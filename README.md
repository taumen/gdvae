
This repository contains an implementation of the GDVAE model for geometric disentanglement of generative latent shape models.

See the paper 
[*Geometric Disentanglement for Generative Latent Shape Models*](http://openaccess.thecvf.com/content_ICCV_2019/html/Aumentado-Armstrong_Geometric_Disentanglement_for_Generative_Latent_Shape_Models_ICCV_2019_paper.html) 
([arxiv](https://arxiv.org/abs/1908.06386)) 
for details, or the
[poster](https://www.cs.toronto.edu/~taumen/pubs/iccv-2019-poster.pdf) for additional visualizations.

## Usage and Setup

### Installation

Use `environment.yml` to create a conda environment called `gdvae`:

```
conda env create -f environment.yml
```

You can use `env_specific.yml` (an export of a working environment) to troubleshoot. 

It has been tested on Ubuntu 16.04 and 18.04, with CUDA 10.

### Data

Using the code requires point clouds along with their Laplace-Beltrami spectra 
(derived either from meshes or the point sets). 
I recommend using a library 
(e.g., the [geometry processing toolbox](https://github.com/alecjacobson/gptoolbox)) for the latter.

Then, instantiate and save an `IEVAE_Dataset` object by calling its constructor with a list of `IEVAE_Datum` objects, and call `save_to_file`. Note it is possible to pass only a mesh object to a datum constructor: a point cloud will be sampled and the spectrum can be estimated from the shape (mesh or point set). See also `generateDatasetFromFolder.py` for reference.

### Downloads

We have provided pretrained models based on data from 
[MPI SMAL](http://smal.is.tue.mpg.de/) and 
[MPI SMPL](https://smpl.is.tue.mpg.de/) 
(via [SURREAL](https://github.com/gulvarol/surreal)).
We also have shape data derived from 
[MNIST](http://yann.lecun.com/exdb/mnist/).

Both of these can be accessed from a download link
[here](https://ln2.sync.com/dl/ae5f7bc00/hjfpuh2u-pxjrzbs5-qhwr2fg2-zq3gzksx). 

## Demos

After cloning the repo, place the downloaded data into a `data` folder and the pretrained models into `models` (see above).
You can test that the installation is working and the downloads are in the right place by running
`python demo.py print_smpl` or 
`python demo.py print_smpl`, 
which should print the architectures and parameters of the respective models. 

### Visualizations

Running 
`python demo.py vis_smpl` or 
`python demo.py vis_smpl`
will visualize 
(1) decoded random latent samples 
and
(2) intrinsic/extrinsic latent interpolations 
(i.e., as in the paper, moving in `z_I` horizontally and `z_E` vertically).

Visualizations will open as interactive OpenGL renders in a pyglet window 
(based on the trimesh lib visualizer).

Note that the visualizations are based on samples from the latent prior. 
Using encoded shapes will tend to generate better point clouds and interpolations, 
in addition to letting you control the interpolation endpoints.
An example is shown just below for visualizing MNIST encodings.

### Training

Training a model occurs in two steps: (1) training the outer autoencoder (AE) and (2) training the VAE on its latent space.

For instance, using the provided MNIST data, this may be done as follows:

 * **Training the AE**: run `python demo.py train_mnist_ae` to train the AE model.
 
 * **Training the VAE**: run `python demo.py train_mnist_vae --ae_model [ae_model_path]` to train the VAE model.

You can then visualize the result with 
```
python demo.py vis_mnist_vae --ae_model [ae_model_path] --vae_model [vae_model_path]
```
which will show 
(1) latent prior samples, 
(2) interpolations between latent prior samples, and
(3) interpolations between encoded (latent posterior) samples.

Note: in general, the training and hyper-parameters of the VAE may depend somewhat on the results of the AE 
(e.g., the scale of the latent space may affect the loss weights).
You may wish to consider tweaking the values within `gdvae_test.py` in the `SETTINGS` variable, to this end.

## License

The code, except of course those parts that are derivatives/modifications of other works with a different license, is [CC BY-NC-SA 3](https://creativecommons.org/licenses/by-nc-sa/3.0/legalcode) ([readable](https://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US)). 

Attribution should be a citation to the originating [GDVAE](https://arxiv.org/abs/1908.06386) work. 

Code that is derived from other work with a different license is marked as such in the comments.

## Contact

See [my website](https://www.cs.toronto.edu/~taumen/).

